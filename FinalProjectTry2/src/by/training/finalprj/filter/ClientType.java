package by.training.finalprj.filter;

/**
 * An enum of possible user types
 */
public enum ClientType {
    GUEST,
    USER,
    ADMIN,
    BLOCKED_USER
}
