package by.training.finalprj.filter;

import by.training.finalprj.manager.ParameterNameEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This is a filter for pages which are admin-only, it doesn't let guests and users
 * redirect to them (sends them to main page instead)
 */
@WebFilter( urlPatterns = { "/jsp/admin/*" },
        initParams = { @WebInitParam(name = "INDEX_PATH", value = "/index.jsp") })
public class AdminPageRedirectSecurityFilter implements Filter {

    private String indexPath;

    public void init(FilterConfig fConfig) throws ServletException {
        indexPath = fConfig.getInitParameter("INDEX_PATH");
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        ClientType clientType;
        if (session.getAttribute(ParameterNameEnum.USER_TYPE.getName()) == null) {
            clientType = ClientType.GUEST;
            session.setAttribute(ParameterNameEnum.USER_TYPE.getName(), clientType);
        } else {
            clientType = ClientType.valueOf(session.getAttribute(ParameterNameEnum.USER_TYPE.getName()).toString());
        }
        if (clientType != ClientType.ADMIN) {
            httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
        }
        chain.doFilter(request, response);
    }

    public void destroy() {
    }
}
