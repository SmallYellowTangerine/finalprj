package by.training.finalprj.filter;

import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.PathPropertyNameEnum;
import by.training.finalprj.manager.PathConfigurationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This is a filter for servlet access, it doesn't let guests
 * access the servlet
 */
@WebFilter(urlPatterns = { "/Controller" }, servletNames = { "Controller" })
public class ServletSecurityFilter implements Filter {

    public void init(FilterConfig fConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession session = httpServletRequest.getSession();
        ClientType clientType;
        if (session.getAttribute(ParameterNameEnum.USER_TYPE.getName()) == null) {
            clientType = ClientType.GUEST;
            session.setAttribute(ParameterNameEnum.USER_TYPE.getName(), clientType);
        } else {
            clientType = ClientType.valueOf(session.getAttribute(ParameterNameEnum.USER_TYPE.getName()).toString());
        }
        if ( (clientType == ClientType.GUEST)
                && (!request.getParameter(ParameterNameEnum.COMMAND.getName()).equals(ParameterNameEnum.LOCALIZE.getName()))
                && (!request.getParameter(ParameterNameEnum.COMMAND.getName()).equals(ParameterNameEnum.LOGIN.getName()))
                && (!request.getParameter(ParameterNameEnum.COMMAND.getName()).equals(ParameterNameEnum.REGISTER.getName())) ) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher(PathConfigurationManager.getProperty(PathPropertyNameEnum.MAIN.getName()));
            dispatcher.forward(httpServletRequest, httpServletResponse);
            return;
        }
        //pass the request along the filter chain
        chain.doFilter(request, response);
    }

    public void destroy() {
    }
}