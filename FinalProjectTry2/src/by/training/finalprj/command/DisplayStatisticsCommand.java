package by.training.finalprj.command;

import by.training.finalprj.entity.User;
import by.training.finalprj.logic.DisplayUsersLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.LoginLogic;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.util.ArrayList;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * DisplayStatisticsCommand is used for showing users statistics (like rating)
 */
public class DisplayStatisticsCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        DisplayUsersLogic displayUsersLogic = new DisplayUsersLogic();
        LoginLogic loginLogic = new LoginLogic();
        try {
            ArrayList<User> allUsers = displayUsersLogic.getAllUsers();
            loginLogic.sortAllUsersByRating(allUsers);
            request.setAttribute(ParameterNameEnum.USERS.getName(), allUsers);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while displaying users", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.STATISTICS.getName());
        }
        return page;
    }
}