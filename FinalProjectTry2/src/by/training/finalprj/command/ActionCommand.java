package by.training.finalprj.command;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

/**
 * This is an interface for the command pattern (commands are basically
 * controllers, since all application is implementation of layered architecture pattern)
 */
public interface ActionCommand {

    String execute(HttpServletRequest request, Session... sessions);
}