package by.training.finalprj.command.factory;

import by.training.finalprj.command.*;
import by.training.finalprj.command.CommandEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.manager.ErrorMessageManager;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

/**
 * This class is a factory (implementing factory method pattern)
 * for defining the command user needs to be executed.
 */
public class ActionFactory {

    private final Logger LOGGER = LogManager.getRootLogger();

    /**
     * This method defines command based on an attribute from request.
     * For non-websocket pages.
     * @param request - client's request
     * @return ActionCommand - command which will be executed
     */
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand currentCommand = new EmptyCommand();
        String commandName = request.getParameter(ParameterNameEnum.COMMAND.getName());
        if (commandName == null || commandName.isEmpty()) {
            return currentCommand;
        }
        try {
            CommandEnum commandEnum = CommandEnum.valueOf(commandName.toUpperCase());
            currentCommand = commandEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Unable to define command " + commandName, e);
            request.setAttribute(ParameterNameEnum.WRONG_ACTION.getName(), commandName
                    + ErrorMessageManager.getProperty(ResultMessageEnum.WRONG_ACTION_MESSAGE.getName()));
        }
        return currentCommand;
    }

    /**
     * This method defines command based on an attribute from websocket session.
     * For websocket pages.
     * @param session - user session object
     * @return ActionCommand - command which will be executed
     */
    public ActionCommand defineCommand(Session session) {
        ActionCommand currentCommand = new EmptyCommand();
        String commandName;
        User user = (User) session.getUserProperties().get(ParameterNameEnum.USER.getName());
        String gameVsAiStart = (String) session.getUserProperties().get(ParameterNameEnum.GAME_VS_AI_START.getName());
        if (session.getUserProperties().get(ParameterNameEnum.MESSAGING.getName()) != null)  {
            return CommandEnum.SEND_MESSAGE.getCurrentCommand();
        }
        if (session.getUserProperties().get(ParameterNameEnum.AI_GAME_OVER.getName()) != null) {
            return CommandEnum.AI_END_GAME.getCurrentCommand();
        }
        if (session.getUserProperties().get(ParameterNameEnum.GAME_OVER.getName()) != null) {
            return CommandEnum.END_GAME.getCurrentCommand();
        }
        if (gameVsAiStart != null) {
            session.getUserProperties().remove(ParameterNameEnum.GAME_VS_AI_START.getName());
            return CommandEnum.AI_WELCOME_PLAYER.getCurrentCommand();
        } else if (user != null) {
            commandName = user.getGamingStatus().toString();
        } else {
            return CommandEnum.REGISTER_PLAYER.getCurrentCommand();
        }
        try {
            CommandEnum commandEnum = CommandEnum.valueOf(commandName.toUpperCase());
            currentCommand = commandEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Unable to define websocket command " + commandName, e);
            session.getUserProperties().put(ParameterNameEnum.WRONG_ACTION.getName(), commandName
                    + ErrorMessageManager.getProperty(ResultMessageEnum.WRONG_ACTION_MESSAGE.getName()));
        }
        return currentCommand;
    }
}