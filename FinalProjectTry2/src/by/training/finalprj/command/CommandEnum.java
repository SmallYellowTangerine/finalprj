package by.training.finalprj.command;

import by.training.finalprj.command.websocket.*;

/**
 * This is an enum used by the command pattern. After user chooses an action,
 * command's name is sent to the servlet;this enum maps command names to command objects
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    LOCALIZE {
        {
            this.command = new LocalizeCommand();
        }
    },
    PUT_MONEY_IN_ACCOUNT {
        {
            this.command = new ReplenishBalanceCommand();
        }
    },
    DISPLAY_USERS {
        {
            this.command = new DisplayUsersCommand();
        }
    },
    REGISTER_PLAYER {
        {
            this.command = new PvpRegisterPlayerCommand();
        }
    },
    MAKING_BET {
        {
            this.command = new PvpMakeBetCommand();
        }
    },
    GAME_STARTED {
        {
            this.command = new PvpStartGameCommand();
        }
    },
    PLAYER_CONTINUES {
        {
            this.command = new PvpStartGameCommand();
        }
    },
    BLOCK {
        {
            this.command = new BlockCommand();
        }
    },
    DISPLAY_DIALOGUES {
        {
            this.command = new DisplayDialoguesCommand();
        }
    },
    SEARCH_DIALOGUE {
        {
            this.command = new SearchDialogueCommand();
        }
    },
    DISPLAY_ADMINS {
        {
            this.command = new DisplayAdminsCommand();
        }
    },
    DISPLAY_MESSAGES {
        {
            this.command = new DisplayMessagesCommand();
        }
    },
    SEND_MESSAGE {
        {
            this.command = new SendMessageCommand();
        }
    },
    END_GAME {
        {
            this.command = new PvpEndGameCommand();
        }
    },
    STATISTICS {
        {
            this.command = new DisplayStatisticsCommand();
        }
    },
    DELETE_USER {
        {
            this.command = new DeleteUserCommand();
        }
    },
    AI_WELCOME_PLAYER {
        {
            this.command = new AiWelcomePlayerCommand();
        }
    },
    AI_GAME_STARTED {
        {
            this.command = new AiStartGameCommand();
        }
    },
    AI_PLAYER_CONTINUES {
        {
            this.command = new AiStartGameCommand();
        }
    },
    AI_END_GAME {
        {
            this.command = new AiEndGameCommand();
        }
    };

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
