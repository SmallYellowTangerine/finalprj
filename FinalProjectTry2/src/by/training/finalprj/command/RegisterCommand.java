package by.training.finalprj.command;

import by.training.finalprj.filter.ClientType;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.RegistrationLogic;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * RegisterCommand is used for registering users
 */
public class RegisterCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        HttpSession session = request.getSession(true);
        String page;
        RegistrationLogic registrationLogic = new RegistrationLogic();
        String login = request.getParameter(ParameterNameEnum.LOGIN.getName());
        String password = request.getParameter(ParameterNameEnum.PASSWORD.getName());
        String email = request.getParameter(ParameterNameEnum.EMAIL.getName());
        String cardNumber = request.getParameter(ParameterNameEnum.CARD.getName());
        try {
            if (registrationLogic.registerUser(login, password, email, cardNumber)) {
                setUserParameters(session, login);
                page = PathConfigurationManager.getProperty(PathPropertyNameEnum.MAIN.getName());
            } else {
                page = setErrorMessage(request);
            }
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while registering user", e);
            page = setErrorMessage(request);
        }
        return page;
    }

    private String setErrorMessage(HttpServletRequest request) {
        String page;
        request.setAttribute(ParameterNameEnum.ERROR_LOGIN_PASSWORD_MESSAGE.getName(),
                ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        page = PathConfigurationManager.getProperty(PathPropertyNameEnum.LOGIN.getName());
        return page;
    }

    private void setUserParameters(HttpSession session, String login) {
        session.setAttribute(ParameterNameEnum.USER.getName(), login);
        session.setAttribute(ParameterNameEnum.LOGGED_IN.getName(), true);
        session.setAttribute(ParameterNameEnum.USER_TYPE.getName(), ClientType.USER.toString());
    }
}