package by.training.finalprj.command;

import by.training.finalprj.entity.Dialogue;
import by.training.finalprj.logic.DialogueLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.util.ArrayList;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * DisplayDialoguesCommand is used for showing a list of existing dialogues to user
 */
public class DisplayDialoguesCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        HttpSession httpSession = request.getSession();
        DialogueLogic dialogueLogic = new DialogueLogic();
        String login = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        try {
            ArrayList<Dialogue> allDialogues = new ArrayList<>(dialogueLogic.getDialogues(login));
            request.setAttribute(ParameterNameEnum.DIALOGUES.getName(), allDialogues);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while displaying dialogues", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.VIEW_DIALOGUES.getName());
        }
        return page;
    }
}