package by.training.finalprj.command;

import by.training.finalprj.manager.PathConfigurationManager;
import by.training.finalprj.manager.PathPropertyNameEnum;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * LogoutCommand is used for de-authorizing users
 */
public class LogoutCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page = PathConfigurationManager.getProperty(PathPropertyNameEnum.INDEX.getName());
        request.getSession().invalidate();
        return page;
    }
}
