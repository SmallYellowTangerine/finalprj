package by.training.finalprj.command;

import by.training.finalprj.entity.Message;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.MessageLogic;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.util.ArrayList;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * DisplayMessagesCommand is used for showing a list of messages in a dialogue to user
 */
public class DisplayMessagesCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        HttpSession httpSession = request.getSession();
        MessageLogic messageLogic = new MessageLogic();
        String username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        String partnerUsername = request.getParameter(ParameterNameEnum.PARTNER_USERNAME.getName());
        httpSession.setAttribute(ParameterNameEnum.PARTNER_USERNAME.getName(), partnerUsername);
        try {
            ArrayList<Message> allMessages = messageLogic.getAllMessages(username, partnerUsername);
            messageLogic.setReadMessages(partnerUsername, allMessages);
            request.setAttribute(ParameterNameEnum.MESSAGES.getName(), allMessages);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while displaying messages", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.VIEW_MESSAGES.getName());
        }
        return page;
    }
}
