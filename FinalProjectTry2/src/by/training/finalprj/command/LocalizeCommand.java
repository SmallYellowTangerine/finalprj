package by.training.finalprj.command;

import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.PathConfigurationManager;
import by.training.finalprj.manager.PathPropertyNameEnum;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * LocalizeCommand is used for localizing text on jsp pages
 */
public class LocalizeCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        String locale = (request.getParameter(ParameterNameEnum.RU.getName()) == null) ? ParameterNameEnum.EN_US.getName() :
                                                                                         ParameterNameEnum.RU_RU.getName();
        HttpSession session = request.getSession(true);
        session.setAttribute(ParameterNameEnum.LOCALE.getName(), locale);
        page = PathConfigurationManager.getProperty(PathPropertyNameEnum.MAIN.getName());
        return page;
    }
}