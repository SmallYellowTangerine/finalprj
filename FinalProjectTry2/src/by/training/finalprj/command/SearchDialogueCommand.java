package by.training.finalprj.command;

import by.training.finalprj.entity.Dialogue;
import by.training.finalprj.logic.DialogueLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.util.ArrayList;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * SearchDialogueCommand is used for searching for users to message
 */
public class SearchDialogueCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        HttpSession session = request.getSession(true);
        String partnerUsername = request.getParameter(ParameterNameEnum.SEARCH_LOGIN.getName());
        String username = (String)session.getAttribute(ParameterNameEnum.USER.getName());
        DialogueLogic dialogueLogic = new DialogueLogic();
        try {
            Dialogue dialogue = dialogueLogic.searchDialogue(username, partnerUsername);
            ArrayList<Dialogue> dialogues = new ArrayList<>();
            dialogues.add(dialogue);
            request.setAttribute(ParameterNameEnum.DIALOGUES.getName(), dialogues);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while searching for dialogue", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.VIEW_DIALOGUES.getName());
        }
        return page;
    }
}