package by.training.finalprj.command;

import by.training.finalprj.logic.DeleteUserLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.manager.ErrorMessageManager;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * DeleteUserCommand is used for deleting user (by administrator).
 */
public class DeleteUserCommand  implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        String resultMessage;
        String username = request.getParameter(ParameterNameEnum.USER_LOGIN.getName());
        DeleteUserLogic deleteUserLogic = new DeleteUserLogic();
        try {
            if (deleteUserLogic.deleteUser(username)) {
                resultMessage = ResultMessageEnum.OPERATION_SUCCESSFUL.getName();
            } else {
                resultMessage = ResultMessageEnum.OPERATION_FAILED.getName();
            }
            request.setAttribute(ParameterNameEnum.OPERATION_RESULT.getName(), resultMessage);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while deleting", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            DisplayUsersCommand displayUsersCommand = new DisplayUsersCommand();
            page = displayUsersCommand.execute(request, sessions);
        }
        return page;
    }
}