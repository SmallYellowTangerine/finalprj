package by.training.finalprj.command;

import by.training.finalprj.entity.User;
import by.training.finalprj.logic.DisplayUsersLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.util.ArrayList;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * DisplayUsersCommand is used for showing a list of users and info about them to the admin
 */
public class DisplayUsersCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        DisplayUsersLogic displayUsersLogic = new DisplayUsersLogic();
        try {
            ArrayList<User> allUsers = displayUsersLogic.getAllUsers();
            request.setAttribute(ParameterNameEnum.USERS.getName(), allUsers);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while displaying users", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.VIEW_USERS.getName());
        }
        return page;
    }
}