package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.PvpBetLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.PvpGameManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.math.BigDecimal;
import static by.training.finalprj.entity.GamingStatusEnum.GAME_STARTED;
import static by.training.finalprj.entity.GamingStatusEnum.MADE_BET;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * PvpMakeBetCommand is used for making bet in pvp (player-vs-player) games
 */
public class PvpMakeBetCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        PvpBetLogic pvpBetLogic = new PvpBetLogic();
        getSessions(sessions);
        try {
            username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        } catch (NullPointerException e) {
            username = ((User)httpSession.getAttribute(ParameterNameEnum.USER.getName())).getLogin();
        }
        try {
            //send it to logic
            BigDecimal betAmount = new BigDecimal(session.getUserProperties().get(ParameterNameEnum.INPUT_MESSAGE.getName()).toString());
            User user = pvpBetLogic.makeBet(username, betAmount);
            resultMessage = determineBetResult(username, user);
        } catch (LogicException e) {
            LOGGER.warn("Exception in betting logic", e);
            resultMessage = ResultMessageEnum.LOGIC_EXCEPTION.getName();
        } catch (NumberFormatException e) {
            LOGGER.info("Something wrong with bet input", e);
            resultMessage = ResultMessageEnum.UNABLE_TO_BET.getName();
        }
        return resultMessage;
    }

    private String determineBetResult(String username, User user) {
        if (user != null) {
            //change status to made bet
            user.setGamingStatus(MADE_BET);
            //generate output
            resultMessage = ResultMessageEnum.WAITING_OTHER_BET.getName();
            //if partner has made bet already, change status to is playing, update output
            if (PvpGameManager.getPartner(user.getCurrentGame(), username).getGamingStatus().equals(MADE_BET)) {
                resultMessage = startGame(username, user);
            }
        } else {
            resultMessage = ResultMessageEnum.UNABLE_TO_BET.getName();
        }
        return resultMessage;
    }

    private String startGame(String username, User user) {
        PvpGameManager.getPartner(user.getCurrentGame(), username).setGamingStatus(GAME_STARTED);
        user.setGamingStatus(GAME_STARTED);
        resultMessage = ResultMessageEnum.GAME_STARTED.getName();
        return resultMessage;
    }
}
