package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.entity.GamingStatusEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.AiWelcomePlayerLogic;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * AiWelcomePlayerCommand is used for welcoming player in a user vs computer game
 */
public class AiWelcomePlayerCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        getSessions(sessions);
        username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        AiWelcomePlayerLogic aiWelcomePlayerLogic = new AiWelcomePlayerLogic();
        try {
            User user = aiWelcomePlayerLogic.registerPlayer(username);
            user.setGamingStatus(GamingStatusEnum.AI_GAME_STARTED);
            if (user != null) {
                session.getUserProperties().put(ParameterNameEnum.USER.getName(), user);
                resultMessage = ResultMessageEnum.AI_WELCOME_MESSAGE.getName();
            } else {
                resultMessage = ResultMessageEnum.UNABLE_TO_REGISTER.getName();
            }
        } catch (LogicException e) {
            LOGGER.warn("Exception in player registering logic during game vs AI", e);
            resultMessage = "Logic exception happened";
        }
        return resultMessage;
    }
}
