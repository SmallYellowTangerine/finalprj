package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.AiGameLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.util.ArrayList;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * AiEndGameCommand is used for ending the game of user vs server correctly
 */
public class AiEndGameCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        getSessions(sessions);
        username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        resultMessage = null;
        AiGameLogic aiGameLogic = new AiGameLogic();
        try {
            User player = (User) session.getUserProperties().get(ParameterNameEnum.USER.getName());
            aiGameLogic.endGame(player);
            clearGameProperties(session, player);
        } catch (NullPointerException e) {
            LOGGER.warn("AI game finished early incorrectly", e);
            resultMessage = ResultMessageEnum.LOGIC_EXCEPTION.getName();
        }
        return resultMessage;
    }

    private void clearGameProperties(Session session, User player) {
        session.getUserProperties().putIfAbsent(ParameterNameEnum.GAME_OVER.getName(), ResultMessageEnum.PLAYER_LEFT.getName());
        session.getUserProperties().remove(ParameterNameEnum.INPUT_MESSAGE.getName());
        player.setCurrentGame(null);
        player.setCurrentBet(null);
        player.setCurrentPoints(null);
        player.setGamingStatus(null);
        player.setDrawnCards(new ArrayList<>());
    }
}
