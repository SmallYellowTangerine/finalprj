package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.entity.MessageStatusEnum;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.MessageLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * SendMessageCommand is used for sending messages from user to user
 */
public class SendMessageCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        getSessions(sessions);
        String partnerUsername;
        String inputMessage;
        MessageLogic messageLogic = new MessageLogic();
        resultMessage = null;
        try {
            username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
            partnerUsername = httpSession.getAttribute(ParameterNameEnum.PARTNER_USERNAME.getName()).toString();
            httpSession.setAttribute(ParameterNameEnum.PARTNER_STATUS.getName(), messageLogic.checkPartnerStatus(username, partnerUsername));
            if (session.getUserProperties().get(ParameterNameEnum.MESSAGING.getName()).equals(MessageStatusEnum.STARTING)) {
                session.getUserProperties().put(ParameterNameEnum.USERNAME.getName(), username);
                return resultMessage;
            }
            if (session.getUserProperties().get(ParameterNameEnum.MESSAGING.getName()).equals(MessageStatusEnum.FINISHING)) {
                return finishMessaging(session, httpSession, username, resultMessage);
            }
            session.getUserProperties().putIfAbsent(ParameterNameEnum.PARTNER_USERNAME.getName(), partnerUsername);
            inputMessage = session.getUserProperties().get(ParameterNameEnum.INPUT_MESSAGE.getName()).toString();
            resultMessage = messageLogic.sendMessage(username, partnerUsername, inputMessage,
                            MessageStatusEnum.valueOf(httpSession.getAttribute(ParameterNameEnum.PARTNER_STATUS.getName()).toString()));
        } catch (LogicException e) {
            LOGGER.warn("Exception in messaging logic", e);
            resultMessage = null;
        }
        return resultMessage;
    }

    private String finishMessaging(Session session, HttpSession httpSession, String username, String message) throws LogicException {
        MessageLogic messageLogic = new MessageLogic();
        session.getUserProperties().remove(ParameterNameEnum.USERNAME.getName());
        session.getUserProperties().remove(ParameterNameEnum.MESSAGING.getName());
        httpSession.removeAttribute(ParameterNameEnum.PARTNER_STATUS.getName());
        httpSession.removeAttribute(ParameterNameEnum.PARTNER_USERNAME.getName());
        messageLogic.removeInterlocutor(username);
        return message;
    }
}