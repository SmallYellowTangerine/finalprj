package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.entity.CardEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.AiGameLogic;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import static by.training.finalprj.entity.GamingStatusEnum.AI_PLAYER_CONTINUES;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * AiEndGameCommand is used for starting/continuing game of user vs server
 */
public class AiStartGameCommand  extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        getSessions(sessions);
        AiGameLogic aiGameLogic = new AiGameLogic();
        String passMessage = "";
        if (session.getUserProperties().get(ParameterNameEnum.INPUT_MESSAGE.getName()) != null) {
            passMessage = session.getUserProperties().get(ParameterNameEnum.INPUT_MESSAGE.getName()).toString();
        }
        try {
            User user = (User) session.getUserProperties().get(ParameterNameEnum.USER.getName());
            if (passMessage.equals(ParameterNameEnum.PASS.getName())) {
                resultMessage = aiGameLogic.aiDrawCards(user);
            } else {
                CardEnum card = aiGameLogic.getRandomCard(user);
                resultMessage = getCardDrawingResult(user, card);
            }
        } catch (LogicException e) {
            LOGGER.warn("Exception in game logic", e);
            resultMessage = ResultMessageEnum.LOGIC_EXCEPTION.getName();
        }
        return resultMessage;
    }

    private String getCardDrawingResult(User user, CardEnum card) {
        String message;
        if (card != null) {
            //if they won or lost, display messages to player and partner
            if ( !user.getGamingStatus().equals(AI_PLAYER_CONTINUES) ) {
                message = ResultMessageEnum.AI_GAME_OVER.getName();
            } else {
                message = ResultMessageEnum.AI_DISPLAY_POINTS.getName();
            }
        } else {
            message = ResultMessageEnum.AI_CARD_DRAWING_ERROR.getName();
        }
        return message;
    }
}
