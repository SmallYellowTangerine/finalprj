package by.training.finalprj.command.websocket;

import by.training.finalprj.manager.ParameterNameEnum;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * This abstract class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * GameCommand is used for defining some common fields & methods for game-related commands
 */
abstract class GameCommand {
    Session session;
    HttpSession httpSession;
    String resultMessage;
    String username;

    void getSessions(Session[] sessions) {
        session = sessions[0];
        httpSession = (HttpSession)session.getUserProperties().get(ParameterNameEnum.HTTPSESSION.getName());
    }
}
