package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.PvpGameManager;
import by.training.finalprj.logic.PvpRegisterPlayerLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import static by.training.finalprj.controller.websocket.PvpGameWebsocketController.clients;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * PvpRegisterPlayerCommand is used for registering players in pvp (player-vs-player) games
 */
public class PvpRegisterPlayerCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        PvpRegisterPlayerLogic registerPlayerLogic = new PvpRegisterPlayerLogic();
        getSessions(sessions);
        username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        try {
            User user = registerPlayerLogic.registerPlayer(username);
            if (user != null) {
                session.getUserProperties().put(ParameterNameEnum.USER.getName(), user);
                //wrap into  configurationmanager and properties file
                resultMessage = ResultMessageEnum.REGISTERED_FIRST.getName();
                if (PvpGameManager.getPartner(user.getCurrentGame(), username) != null) {
                    session.getUserProperties().put(ParameterNameEnum.PARTNER.getName(), PvpGameManager
                                                    .getPartner(user.getCurrentGame(), username));
                    for (Session client : clients) {
                        if ( (User) client.getUserProperties().get(ParameterNameEnum.USER.getName())
                                == PvpGameManager.getPartner(user.getCurrentGame(), username)) {
                            client.getUserProperties().putIfAbsent(ParameterNameEnum.PARTNER.getName(), user);
                        }
                    }
                    resultMessage = ResultMessageEnum.REGISTERED_SECOND.getName();
                }
            } else {
                resultMessage = ResultMessageEnum.UNABLE_TO_REGISTER.getName();
            }
        } catch (LogicException e) {
            LOGGER.warn("Exception in player registering logic", e);
            resultMessage = "Logic exception happened";
        }
        return resultMessage;
    }
}
