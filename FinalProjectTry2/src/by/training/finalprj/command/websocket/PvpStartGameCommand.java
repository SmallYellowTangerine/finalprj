package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import by.training.finalprj.entity.CardEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.PvpGameLogic;
import by.training.finalprj.logic.PvpGameManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

import static by.training.finalprj.entity.GamingStatusEnum.PASSED;
import static by.training.finalprj.entity.GamingStatusEnum.PLAYER_CONTINUES;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * PvpStartGameCommand is used for starting/continuing actual pvp (player-vs-player) games
 */
public class PvpStartGameCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        getSessions(sessions);
        PvpGameLogic pvpGameLogic = new PvpGameLogic();
        username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        String passMessage = session.getUserProperties().get(ParameterNameEnum.INPUT_MESSAGE.getName()).toString();
        try {
            User player = PvpGameManager.getPlayer(username);
            User partner = PvpGameManager.getPartner(player.getCurrentGame(), username);
            session.getUserProperties().putIfAbsent(ParameterNameEnum.PARTNER.getName(), partner);
            if (passMessage.equals(ParameterNameEnum.PASS.getName())) {
                resultMessage = passMoveToPartner(session, username);
                session.getUserProperties().putIfAbsent(ParameterNameEnum.PARTNER_PASSED.getName(), ParameterNameEnum.PARTNER_PASSED.getName());
            } else {
                CardEnum card = pvpGameLogic.getRandomCard(username);
                resultMessage = getCardDrawingResult(username, player, partner, card);
            }
        } catch (LogicException e) {
            LOGGER.warn("Exception in game logic", e);
            resultMessage = ResultMessageEnum.LOGIC_EXCEPTION.getName();
        }
        return resultMessage;
    }

    private String passMoveToPartner(Session session, String username) {
        User partner;
        User player;
        String message;
        partner = (User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName());
        player = PvpGameManager.getPlayer(username);
        //set the new status
        partner.setGamingStatus(PLAYER_CONTINUES);
        player.setGamingStatus(PASSED);
        //switch them in session
        session.getUserProperties().put(ParameterNameEnum.USER.getName(), partner);
        session.getUserProperties().put(ParameterNameEnum.PARTNER.getName(), player);
        message = ResultMessageEnum.PASSED.getName();
        return message;
    }

    private String getCardDrawingResult(String username, User player, User partner, CardEnum card) {
        String message;
        if (card != null) {
            message = ResultMessageEnum.DISPLAY_POINTS.getName();
            //if they won or lost, display messages to player and partner
            if (!PvpGameManager.getPlayer(username).getGamingStatus()
                    .equals(PLAYER_CONTINUES)) {
                message = ResultMessageEnum.GAME_OVER.getName();
                //remove the game from the list of games
                PvpGameManager.getAllGames().remove(player.getCurrentGame());
                player.setCurrentGame(null);
                partner.setCurrentGame(null);
            }
        } else {
            message = ResultMessageEnum.CARD_DRAWING_ERROR.getName();
        }
        return message;
    }
}
