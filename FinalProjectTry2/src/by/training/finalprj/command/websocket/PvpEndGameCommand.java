package by.training.finalprj.command.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.entity.GamingStatusEnum;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.PvpGameLogic;
import by.training.finalprj.logic.PvpGameManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;
import java.util.ArrayList;
import static by.training.finalprj.entity.Game.GameStatus.FINISHED_CORRECTLY;
import static by.training.finalprj.entity.Game.GameStatus.UNFINISHED;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from websocket session object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * PvpEndGameCommand is used for ending pvp (player-vs-player) games correctly
 */
public class PvpEndGameCommand extends GameCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        getSessions(sessions);
        username = httpSession.getAttribute(ParameterNameEnum.USER.getName()).toString();
        resultMessage = null;
        PvpGameLogic pvpGameLogic = new PvpGameLogic();
        try {
            User partner = null;
            User player = (User) session.getUserProperties().get(ParameterNameEnum.USER.getName());
            if (player.getCurrentGame() != null) {
                partner = PvpGameManager.getPartner(player.getCurrentGame(), username);
            } else if (session.getUserProperties().get(ParameterNameEnum.PARTNER.getName()) != null) {
                partner = (User) session.getUserProperties().get(ParameterNameEnum.PARTNER.getName());
            }
            if (player.getGamingStatus() != null &&
                    player.getGamingStatus().equals(GamingStatusEnum.PARTNER_LEFT)) {
                player.setGamingStatus(null);
                return null;
            }
            if (partner != null) {
                session.getUserProperties().putIfAbsent(ParameterNameEnum.PARTNER.getName(), partner);
            }
            //if status already is unfinished cause the other person left then we just quietly leave
            //if the game is finished, same
            if ( player.getCurrentGame() != null ) {
                if ( !(player.getCurrentGame().getGameStatus().equals(FINISHED_CORRECTLY)
                        || player.getCurrentGame().getGameStatus().equals(UNFINISHED))) {
                    pvpGameLogic.endGameEarly(player, partner);
                    //inform partner about it
                    resultMessage = ResultMessageEnum.PLAYER_LEFT.getName();
                }
            }
            //if everything's normal we return null, we need to prepare for this in controller!!!
            clearGameProperties(session, player, partner);
        } catch (NullPointerException | LogicException e) {
            LOGGER.warn("Game finished early incorrectly", e);
            resultMessage = ResultMessageEnum.LOGIC_EXCEPTION.getName();
        }
        return resultMessage;
    }

    private void clearGameProperties(Session session, User player, User partner) {
        session.getUserProperties().putIfAbsent(ParameterNameEnum.GAME_OVER.getName(), ResultMessageEnum.PLAYER_LEFT.getName());
        session.getUserProperties().remove(ParameterNameEnum.INPUT_MESSAGE.getName());
        player.setCurrentGame(null);
        player.setCurrentBet(null);
        player.setCurrentPoints(null);
        player.setGamingStatus(null);
        player.setDrawnCards(new ArrayList<>());
        if (partner != null) {
            partner.setCurrentGame(null);
            partner.setCurrentBet(null);
            partner.setCurrentPoints(null);
            partner.setDrawnCards(new ArrayList<>());
        }
        //we don't set partner's gamin status to null cause we're gonna need it still
    }
}
