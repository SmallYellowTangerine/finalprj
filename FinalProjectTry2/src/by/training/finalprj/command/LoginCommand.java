package by.training.finalprj.command;

import by.training.finalprj.filter.ClientType;
import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.LoginLogic;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * LoginCommand is used for users' authorization
 */
public class LoginCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        HttpSession session = request.getSession(true);
        String page;
        String login = request.getParameter(ParameterNameEnum.LOGIN.getName());
        String password = request.getParameter(ParameterNameEnum.PASSWORD.getName());
        LoginLogic loginLogic = new LoginLogic();
        try {
            if (loginLogic.checkLogin(login, password)) {
                session.setAttribute(ParameterNameEnum.USER.getName(), login);
                session.setAttribute(ParameterNameEnum.LOGGED_IN.getName(), true);
                setUserType(session, login);
                page = PathConfigurationManager.getProperty(PathPropertyNameEnum.MAIN.getName());
            } else {
                page = setErrorMessage(request);
            }
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while authorizing user", e);
            page = setErrorMessage(request);
        }
        return page;
    }

    private String setErrorMessage(HttpServletRequest request) {
        String page;
        request.setAttribute(ParameterNameEnum.ERROR_LOGIN_PASSWORD_MESSAGE.getName(),
                ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        page = PathConfigurationManager.getProperty(PathPropertyNameEnum.LOGIN.getName());
        return page;
    }

    private void setUserType(HttpSession session, String login) throws LogicException {
        LoginLogic loginLogic = new LoginLogic();
        if (loginLogic.checkAdmin(login)) {
            session.setAttribute(ParameterNameEnum.USER_TYPE.getName(), ClientType.ADMIN.toString());
        } else if (loginLogic.checkBlocked(login)) {
            session.setAttribute(ParameterNameEnum.USER_TYPE.getName(), ClientType.BLOCKED_USER.toString());
        } else {
            session.setAttribute(ParameterNameEnum.USER_TYPE.getName(), ClientType.USER.toString());
        }
    }
}
