package by.training.finalprj.command;

import by.training.finalprj.logic.LogicException;
import by.training.finalprj.logic.ReplenishBalanceLogic;
import by.training.finalprj.manager.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * ReplenishBalanceCommand is used for replenishing user's balance (putting "money" on their account)
 */
public class ReplenishBalanceCommand implements ActionCommand {

    private final Logger LOGGER = LogManager.getRootLogger();

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        String resultMessage;
        ReplenishBalanceLogic replenishBalanceLogic = new ReplenishBalanceLogic();
        HttpSession session = request.getSession(true);
        String sum = request.getParameter(ParameterNameEnum.SUM.getName());
        String password = request.getParameter(ParameterNameEnum.PASSWORD.getName());
        String username = session.getAttribute(ParameterNameEnum.USER.getName()).toString();
        try {
            if (replenishBalanceLogic.replenishBalance(username, password, sum)) {
                resultMessage = ResultMessageEnum.OPERATION_SUCCESSFUL.getName();
            } else {
                resultMessage = ResultMessageEnum.OPERATION_FAILED.getName();
            }
            request.setAttribute(ParameterNameEnum.OPERATION_RESULT.getName(), resultMessage);
        } catch (LogicException e) {
            LOGGER.warn("Logic exception while replenishing balance", e);
            request.setAttribute(ParameterNameEnum.ERROR_OPERATION_MESSAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName()));
        } finally {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.REPLENISH_BALANCE.getName());
        }
        return page;
    }
}

