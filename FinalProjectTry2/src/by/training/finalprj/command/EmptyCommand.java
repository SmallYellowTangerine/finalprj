package by.training.finalprj.command;

import by.training.finalprj.manager.PathConfigurationManager;
import by.training.finalprj.manager.PathPropertyNameEnum;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.Session;

/**
 * This class is one of the command pattern commands, used for extracting
 * parameters from request object, sending them to business logic, and
 * getting and processing result of business logic operations.
 * EmptyCommand is the default command that does nothing
 */
public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request, Session... sessions) {
        String page;
        if (sessions.length > 0) {
            page = "";
        } else {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.MAIN.getName());
        }
        return page;
    }
}
