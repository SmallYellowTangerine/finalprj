package by.training.finalprj.controller;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.PathPropertyNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import by.training.finalprj.command.factory.*;
import by.training.finalprj.manager.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This is the main servlet
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String page;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);
        page = command.execute(request);
        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = PathConfigurationManager.getProperty(PathPropertyNameEnum.INDEX.getName());
            session.setAttribute(ParameterNameEnum.NULL_PAGE.getName(),
                    ErrorMessageManager.getProperty(ResultMessageEnum.NULL_PAGE_MESSAGE.getName()));
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}
