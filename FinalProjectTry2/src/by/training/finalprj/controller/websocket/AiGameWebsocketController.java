package by.training.finalprj.controller.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.command.factory.ActionFactory;
import by.training.finalprj.manager.AiGameResponseManager;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.ResultMessageEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This is a user vs AI(server) games websocket controller.
 */
@ServerEndpoint(value="/aigamewebsocket", configurator = GetHttpSessionConfigurator.class)
public class AiGameWebsocketController {

    private final static Logger LOGGER = LogManager.getRootLogger();

    private static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());

    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException {
        processSession(session, message);
    }

    @OnOpen
    public void onOpen (Session session, EndpointConfig config) {
        // Add session to the connected sessions set
        clients.add(session);
        //so that we can define command in action factory
        session.getUserProperties().putIfAbsent(ParameterNameEnum.GAME_VS_AI_START.getName(), ParameterNameEnum.GAME_VS_AI_START.getName());
        processSession(session);
    }

    @OnClose
    public void onClose (Session session) {
        session.getUserProperties().put(ParameterNameEnum.AI_GAME_OVER.getName(), ParameterNameEnum.PLAYER_LEFT.getName());
        processSession(session);
        // Remove session from the connected sessions set
        clients.remove(session);
    }

    private static void processSession(Session session, String... messages) {
        if (messages.length > 0) {
            session.getUserProperties().put(ParameterNameEnum.INPUT_MESSAGE.getName(), messages[0]);
        }
        String messageKey;
        ActionFactory actionFactory = new ActionFactory();
        ActionCommand command = actionFactory.defineCommand(session);
        messageKey = command.execute(null, session);
        if (messageKey == null && session.getUserProperties().get(ParameterNameEnum.AI_GAME_OVER.getName()) == null) {
            messageKey = ResultMessageEnum.ERROR.getName();
        } else if (messageKey != null) {
            sendMessageToUser(session, messageKey);
        }
    }

    private static void sendMessageToUser(Session session, String messageKey) {
        for (Session client : clients) {
            if (client.equals(session)) {
                try {
                    client.getBasicRemote().sendText(AiGameResponseManager.getProperty(messageKey, session));
                } catch (IOException e) {
                    LOGGER.warn("Something went wrong when displaying message to player");
                }
            }
        }
    }
}
