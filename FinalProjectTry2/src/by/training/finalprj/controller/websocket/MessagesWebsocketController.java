package by.training.finalprj.controller.websocket;

/**
 * Created by Администратор on 19.07.2017.
 */
import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.command.factory.ActionFactory;
import by.training.finalprj.entity.MessageStatusEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value="/messagewebsocket", configurator = GetHttpSessionConfigurator.class)
public class MessagesWebsocketController {

    private final static Logger LOGGER = LogManager.getRootLogger();

    private static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());

    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException {
        processSession(session, message);
    }

    @OnOpen
    public void onOpen (Session session, EndpointConfig config) {
        // Add session to the connected sessions set
        clients.add(session);
        processSession(session);
    }

    @OnClose
    public void onClose (Session session) {
        // Remove session from the connected sessions set
        session.getUserProperties().put(ParameterNameEnum.MESSAGING.getName(), MessageStatusEnum.FINISHING);
        processSession(session);
        clients.remove(session);
    }

    private static void processSession(Session session, String... messages) {
        determineMessagingStatus(session, messages);
        String newMessage;
        ActionFactory actionFactory = new ActionFactory();
        ActionCommand command = actionFactory.defineCommand(session);
        newMessage = command.execute(null, session);
        //sending message
        if (newMessage != null) {
            for (Session client : clients){
                //send message to the current user
                showMessageToCurrentUser(session, newMessage, client);
                //send message to current user's partner
                showMessageToCurrentPartner(session, newMessage, client);
            }
        } else {
            if (!(session.getUserProperties().containsKey(ParameterNameEnum.MESSAGING.getName())
                    && session.getUserProperties().get(ParameterNameEnum.MESSAGING.getName()).equals(MessageStatusEnum.STARTING))) {
                clients.remove(session);
            }
        }
    }

    private static void showMessageToCurrentPartner(Session session, String newMessage, Session client) {
        if (session.getUserProperties().get(ParameterNameEnum.PARTNER_USERNAME.getName()) != null
                && client.getUserProperties().get(ParameterNameEnum.USERNAME.getName())
                .equals(session.getUserProperties().get(ParameterNameEnum.PARTNER_USERNAME.getName()))) {
            try {
                client.getBasicRemote().sendText(newMessage);
            } catch (IOException e) {
                LOGGER.warn("Something went wrong when displaying message to receiver");
            }
        }
    }

    private static void showMessageToCurrentUser(Session session, String newMessage, Session client) {
        if (client.equals(session)){
            try {
                client.getBasicRemote().sendText(newMessage);
            } catch (IOException e) {
                LOGGER.warn("Something went wrong when displaying message to sender");
            }
        }
    }

    private static void determineMessagingStatus(Session session, String[] messages) {
        if (!(session.getUserProperties().containsKey(ParameterNameEnum.MESSAGING.getName())
                    && session.getUserProperties().get(ParameterNameEnum.MESSAGING.getName()).equals(MessageStatusEnum.FINISHING)) ) {
            if (messages.length == 0) {
                session.getUserProperties().put(ParameterNameEnum.MESSAGING.getName(), MessageStatusEnum.STARTING);
            } else {
                session.getUserProperties().put(ParameterNameEnum.MESSAGING.getName(), MessageStatusEnum.SENDING);
                session.getUserProperties().put(ParameterNameEnum.INPUT_MESSAGE.getName(), messages[0]);
            }
        }
    }
}

