package by.training.finalprj.controller.websocket;

import by.training.finalprj.command.ActionCommand;
import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.command.factory.ActionFactory;
import by.training.finalprj.entity.User;
import by.training.finalprj.manager.WsPartnerResponseManager;
import by.training.finalprj.manager.WsPlayerResponseManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

import static by.training.finalprj.entity.GamingStatusEnum.PASSED;

/**
 * This is a user vs user games websocket controller.
 */
@ServerEndpoint(value="/websocket", configurator = GetHttpSessionConfigurator.class)
public class PvpGameWebsocketController {

    private final static Logger LOGGER = LogManager.getRootLogger();

    public static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());

    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException {
        processSession(session, message);
    }

    @OnOpen
    public void onOpen (Session session, EndpointConfig config) {
        // Add session to the connected sessions set
        clients.add(session);
        processSession(session);
    }

    @OnClose
    public void onClose (Session session) {
        session.getUserProperties().put(ParameterNameEnum.GAME_OVER.getName(), ParameterNameEnum.PLAYER_LEFT.getName());
        processSession(session);
        // Remove session from the connected sessions set
        clients.remove(session);
    }

    private static void processSession(Session session, String... messages) {
        if (messages.length > 0) {
            session.getUserProperties().put(ParameterNameEnum.INPUT_MESSAGE.getName(), messages[0]);
        }
        String messageKey;
        ActionFactory actionFactory = new ActionFactory();
        ActionCommand command = actionFactory.defineCommand(session);
        messageKey = command.execute(null, session);
        if (messageKey != null) {
            for (Session client : clients){
                //send message to the current user
                sendMessageToCurrentPlayer(session, messageKey, client);
                //send message to current user's partner
                sendMessageToCurrentPartner(session, messageKey, client);
                //if player passed - message to partner
                sendPlayerPassedMessage(session, messageKey, client);
            }
        } else if (session.getUserProperties().get(ParameterNameEnum.GAME_OVER.getName()) == null) {
            for (Session client : clients){
                sendErrorMessage(session, client);
            }
        }
    }

    private static void sendErrorMessage(Session session, Session client) {
        if (client.equals(session)) {
            try {
                client.getBasicRemote().sendText("Error while processing session");
            } catch (IOException e) {
                LOGGER.warn("Something went wrong during the game");
            }
        }
    }

    private static void sendPlayerPassedMessage(Session session, String messageKey, Session client) {
        if ( !client.equals(session)) {
            //cheking for null
            if (session.getUserProperties().get(ParameterNameEnum.PARTNER.getName()) != null
                    //checking that the player has indeed passed the move
                    && (((User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName())).getGamingStatus() == PASSED
                        || client.getUserProperties().get(ParameterNameEnum.PARTNER_PASSED.getName()) != null)
                    //and checking that the partner is this player's partner
                    && ( ((User)session.getUserProperties().get(ParameterNameEnum.USER.getName())).getLogin()
                            .equals( ((User)client.getUserProperties().get(ParameterNameEnum.USER.getName())).getLogin() ))) {
                try {
                    client.getBasicRemote().sendText(WsPartnerResponseManager.getProperty(messageKey, session));
                } catch (IOException e) {
                    LOGGER.warn("Something went wrong when displaying player passed message");
                }
            }
        }
    }

    private static void sendMessageToCurrentPartner(Session session, String messageKey, Session client) {
        if (session.getUserProperties().get(ParameterNameEnum.PARTNER.getName()) != null
                && client.getUserProperties().get(ParameterNameEnum.USER.getName())
                                .equals(session.getUserProperties().get(ParameterNameEnum.PARTNER.getName()))) {
            try {
                client.getBasicRemote().sendText(WsPartnerResponseManager.getProperty(messageKey, session));
            } catch (IOException e) {
                LOGGER.warn("Something went wrong when displaying message to partner");
            }
        }
    }

    private static void sendMessageToCurrentPlayer(Session session, String messageKey, Session client) {
        if (client.equals(session) && !messageKey.equals(ParameterNameEnum.PLAYER_LEFT_EARLY.getName())) {
            try {
                client.getBasicRemote().sendText(WsPlayerResponseManager.getProperty(messageKey, session));
            } catch (IOException e) {
                LOGGER.warn("Something went wrong when displaying message to player");
            }
        }
    }
}
