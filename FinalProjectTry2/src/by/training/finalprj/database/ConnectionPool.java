package by.training.finalprj.database;

import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.manager.DatabaseManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This is a connection pool for database access implementation
 */
public class ConnectionPool {

    private static final Logger LOGGER = LogManager.getRootLogger();

    private static ReentrantLock lock = new ReentrantLock();

    private static final String URL = DatabaseManager.getProperty(ParameterNameEnum.DB_URL.getName());
    private static final String PASSWORD = DatabaseManager.getProperty(ParameterNameEnum.DB_PASSWORD.getName());
    private static final String USERNAME = DatabaseManager.getProperty(ParameterNameEnum.DB_USERNAME.getName());

    private int poolSize = Integer.parseInt(DatabaseManager.getProperty(ParameterNameEnum.DB_POOL_SIZE.getName()));

    private BlockingQueue<PooledConnection> freeConnections;
    private Set<PooledConnection> usedConnections;

    private static ConnectionPool instance;
    private static AtomicBoolean atomicBoolean = new AtomicBoolean();

    private ConnectionPool() {
        freeConnections = new ArrayBlockingQueue<>(poolSize);
        usedConnections = Collections.synchronizedSet(new HashSet<>(poolSize));
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            throw new RuntimeException("Couldn't get driver", e);
        }
        for (int i = 0; i < poolSize; i++) {
            try {
                Connection connection =  DriverManager.getConnection(URL, USERNAME, PASSWORD);
                freeConnections.add(new PooledConnection(connection));
            } catch (SQLException e) {
                throw new RuntimeException( "Driver couldn't get connection", e);
            }
        }
        if (freeConnections.size()!= poolSize){
            throw new RuntimeException( "Not all connections created successfully");
        }
    }

    public static ConnectionPool getInstance() {
        if (!atomicBoolean.get()) {
            lock.lock();
            if (instance == null) {
                instance = new ConnectionPool();
                atomicBoolean.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    public Connection getConnection() {
        PooledConnection connection = null;
        try {
            connection = freeConnections.poll(1, TimeUnit.SECONDS);
            if (connection == null) {
                LOGGER.error("Couldn't get connection from freeconnections");
            }
            usedConnections.add(connection);
        } catch (InterruptedException e) {
            LOGGER.error("Interrupted while trying to get connection from freeconnections");
        }
        return connection;
    }

    public void freeConnection(PooledConnection pooledConnection) {
        usedConnections.remove(pooledConnection);
        freeConnections.offer(pooledConnection);
    }

    public void destroy() throws ConnectionPoolException {
        //free connections
        for (int i=0; i < freeConnections.size(); i++ ) {
            try {
                freeConnections.take();
            } catch (InterruptedException e) {
                throw new ConnectionPoolException("Can't close connections", e);
            }
        }
        //de-register drivers
        try {
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                Driver driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
            }
        } catch (SQLException e) {
            LOGGER.error("DriverManager wasn't found.", e);
        }
    }
}