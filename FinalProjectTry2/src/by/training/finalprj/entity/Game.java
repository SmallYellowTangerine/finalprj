package by.training.finalprj.entity;

import java.util.concurrent.ConcurrentHashMap;

/**
 * This is a game entity
 */
public class Game extends Entity {

    public enum GameStatus {
        CREATED,
        STARTED,
        FINISHED_CORRECTLY,
        UNFINISHED
    }

    private ConcurrentHashMap<String, User> players = new ConcurrentHashMap<>();
    private GameStatus gameStatus;
    private User winner;

    public Game() {
        super();
    }

    public Game(Integer id, ConcurrentHashMap<String, User> players) {
        super(id);
        this.players = players;
    }

    public Game(ConcurrentHashMap<String, User> players, GameStatus gameStatus) {
        this.players = players;
        this.gameStatus = gameStatus;
    }

    public ConcurrentHashMap<String, User> getPlayers() {
        return players;
    }

    public void setPlayers(ConcurrentHashMap<String, User> players) {
        this.players = players;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Game)) return false;
        Game game = (Game) o;
        if (getPlayers() != null ? !getPlayers().equals(game.getPlayers()) : game.getPlayers() != null) return false;
        if (getGameStatus() != game.getGameStatus()) return false;
        return getWinner() != null ? getWinner().equals(game.getWinner()) : game.getWinner() == null;
    }

    @Override
    public int hashCode() {
        int result = getPlayers() != null ? getPlayers().hashCode() : 0;
        result = 31 * result + (getGameStatus() != null ? getGameStatus().hashCode() : 0);
        result = 31 * result + (getWinner() != null ? getWinner().hashCode() : 0);
        return result;
    }
}
