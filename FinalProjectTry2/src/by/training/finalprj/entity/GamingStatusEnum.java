package by.training.finalprj.entity;

/**
 * This is an enum of all possible user's gaming statuses
 */
public enum GamingStatusEnum {
    REGISTERED_FIRST,
    MAKING_BET,
    MADE_BET,
    GAME_STARTED,
    PLAYER_WON,
    PLAYER_LOST,
    PLAYER_CONTINUES,
    PASSED,
    PLAYER_LEFT,
    PARTNER_LEFT,
    AI_GAME_STARTED,
    AI_GAME_PLAYER_CONTINUES,
    AI_PLAYER_CONTINUES,
    AI_GAME_USER_WON,
    AI_GAME_USER_LOST;
}
