package by.training.finalprj.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * This is a user entity
 */
public class User extends Entity {

    private String login;
    private String password;
    private String email;
    private LocalDate registrationDate;
    private Integer role;

    private String bankCardNumber;
    private BigDecimal balance;
    private Double rating;
    private boolean blocked;
    private int gamesWon;
    private int gamesLost;

    private GamingStatusEnum gamingStatus;
    private Game currentGame;
    private Integer currentPoints;
    private ArrayList<CardEnum> drawnCards;
    private BigDecimal currentBet;

    public User() {
        super();
        currentPoints = new Integer(0);
        drawnCards = new ArrayList<CardEnum>();
        balance = new BigDecimal(0);
        rating = new Double(0);
    }

    public User(int id) {
        super(id);
        currentPoints = new Integer(0);
        drawnCards = new ArrayList<CardEnum>();
        balance = new BigDecimal(0);
        rating = new Double(0);
    }

    public User(String login, String password, String email, LocalDate registrationDate, Integer role) {
        super();
        this.login = login;
        this.password = password;
        this.email = email;
        this.registrationDate = registrationDate;
        this.role = role;
        currentPoints = new Integer(0);
        drawnCards = new ArrayList<CardEnum>();
        balance = new BigDecimal(0);
        rating = new Double(0);
    }

    public User(String login, String password, String email, LocalDate registrationDate, Integer role, String bankCardNumber,
                BigDecimal balance, double rating, boolean blocked, int gamesWon, int gamesLost) {
        super();
        this.login = login;
        this.password = password;
        this.email = email;
        this.registrationDate = registrationDate;
        this.role = role;
        this.bankCardNumber = bankCardNumber;
        this.balance = balance;
        this.rating = rating;
        this.blocked = blocked;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
        currentPoints = new Integer(0);
        drawnCards = new ArrayList<CardEnum>();
    }

    public User(Integer id, String login, String password, String email, LocalDate registrationDate, Integer role,
                String bankCardNumber, BigDecimal balance, double rating, boolean blocked, int gamesWon, int gamesLost) {
        super(id);
        this.login = login;
        this.password = password;
        this.email = email;
        this.registrationDate = registrationDate;
        this.role = role;
        this.bankCardNumber = bankCardNumber;
        this.balance = balance;
        this.rating = rating;
        this.blocked = blocked;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
        currentPoints = new Integer(0);
        drawnCards = new ArrayList<CardEnum>();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer admin) {
        role = admin;
    }

    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getGamesLost() {
        return gamesLost;
    }

    public void setGamesLost(int gamesLost) {
        this.gamesLost = gamesLost;
    }

    public GamingStatusEnum getGamingStatus() {
        return gamingStatus;
    }

    public void setGamingStatus(GamingStatusEnum gamingStatus) {
        this.gamingStatus = gamingStatus;
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }

    public Integer getCurrentPoints() {
        return currentPoints;
    }

    public void setCurrentPoints(Integer currentPoints) {
        this.currentPoints = currentPoints;
    }

    public ArrayList<CardEnum> getDrawnCards() {
        return drawnCards;
    }

    public void setDrawnCards(ArrayList<CardEnum> drawnCards) {
        this.drawnCards = drawnCards;
    }

    public BigDecimal getCurrentBet() {
        return currentBet;
    }

    public void setCurrentBet(BigDecimal currentBet) {
        this.currentBet = currentBet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (Double.compare(user.getRating(), getRating()) != 0) return false;
        if (isBlocked() != user.isBlocked()) return false;
        if (getGamesWon() != user.getGamesWon()) return false;
        if (getGamesLost() != user.getGamesLost()) return false;
        if (!getLogin().equals(user.getLogin())) return false;
        if (!getPassword().equals(user.getPassword())) return false;
        if (!getEmail().equals(user.getEmail())) return false;
        if (getRegistrationDate() != null ? !getRegistrationDate().equals(user.getRegistrationDate()) : user.getRegistrationDate() != null)
            return false;
        if (!getRole().equals(user.getRole())) return false;
        if (!getBankCardNumber().equals(user.getBankCardNumber())) return false;
        if (getBalance() != null ? !getBalance().equals(user.getBalance()) : user.getBalance() != null) return false;
        if (getGamingStatus() != user.getGamingStatus()) return false;
        if (getCurrentGame() != null ? !getCurrentGame().equals(user.getCurrentGame()) : user.getCurrentGame() != null)
            return false;
        if (getCurrentPoints() != null ? !getCurrentPoints().equals(user.getCurrentPoints()) : user.getCurrentPoints() != null)
            return false;
        if (getDrawnCards() != null ? !getDrawnCards().equals(user.getDrawnCards()) : user.getDrawnCards() != null)
            return false;
        return getCurrentBet() != null ? getCurrentBet().equals(user.getCurrentBet()) : user.getCurrentBet() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getLogin().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getEmail().hashCode();
        result = 31 * result + (getRegistrationDate() != null ? getRegistrationDate().hashCode() : 0);
        result = 31 * result + getRole().hashCode();
        result = 31 * result + getBankCardNumber().hashCode();
        result = 31 * result + (getBalance() != null ? getBalance().hashCode() : 0);
        temp = Double.doubleToLongBits(getRating());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (isBlocked() ? 1 : 0);
        result = 31 * result + getGamesWon();
        result = 31 * result + getGamesLost();
        result = 31 * result + (getGamingStatus() != null ? getGamingStatus().hashCode() : 0);
        result = 31 * result + (getCurrentGame() != null ? getCurrentGame().hashCode() : 0);
        result = 31 * result + (getCurrentPoints() != null ? getCurrentPoints().hashCode() : 0);
        result = 31 * result + (getDrawnCards() != null ? getDrawnCards().hashCode() : 0);
        result = 31 * result + (getCurrentBet() != null ? getCurrentBet().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", registrationDate=" + registrationDate +
                ", role=" + role +
                ", bankCardNumber='" + bankCardNumber + '\'' +
                ", balance=" + balance +
                ", rating=" + rating +
                ", blocked=" + blocked +
                ", gamesWon=" + gamesWon +
                ", gamesLost=" + gamesLost +
                '}';
    }
}
