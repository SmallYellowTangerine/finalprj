package by.training.finalprj.entity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * This is an enum of all the cards for games
 */
public enum CardEnum {
    SIX_OF_CLUBS(6, "/img/cards/6_of_clubs.png"),
    SEVEN_OF_CLUBS(7, "/img/cards/7_of_clubs.png"),
    EIGHT_OF_CLUBS(8, "/img/cards/8_of_clubs.png"),
    NINE_OF_CLUBS(9, "/img/cards/9_of_clubs.png"),
    TEN_OF_CLUBS(10, "/img/cards/10_of_clubs.png"),
    JACK_OF_CLUBS(2, "/img/cards/jack_of_clubs.png"),
    QUEEN_OF_CLUBS(3, "/img/cards/queen_of_clubs.png"),
    KING_OF_CLUBS(4, "/img/cards/king_of_clubs.png"),
    ACE_OF_CLUBS(11, "/img/cards/ace_of_clubs.png"),

    SIX_OF_DIAMONDS(6, "/img/cards/6_of_diamonds.png"),
    SEVEN_OF_DIAMONDS(7, "/img/cards/7_of_diamonds.png"),
    EIGHT_OF_DIAMONDS(8, "/img/cards/8_of_diamonds.png"),
    NINE_OF_DIAMONDS(9, "/img/cards/9_of_diamonds.png"),
    TEN_OF_DIAMONDS(10, "/img/cards/10_of_diamonds.png"),
    JACK_OF_DIAMONDS(2, "/img/cards/jack_of_diamonds.png"),
    QUEEN_OF_DIAMONDS(3, "/img/cards/queen_of_diamonds.png"),
    KING_OF_DIAMONDS(4, "/img/cards/king_of_diamonds.png"),
    ACE_OF_DIAMONDS(11, "/img/cards/ace_of_diamonds.png"),

    SIX_OF_HEARTS(6, "/img/cards/6_of_hearts.png"),
    SEVEN_OF_HEARTS(7, "/img/cards/7_of_hearts.png"),
    EIGHT_OF_HEARTS(8, "/img/cards/8_of_hearts.png"),
    NINE_OF_HEARTS(9, "/img/cards/9_of_hearts.png"),
    TEN_OF_HEARTS(10, "/img/cards/10_of_hearts.png"),
    JACK_OF_HEARTS(2, "/img/cards/jack_of_hearts.png"),
    QUEEN_OF_HEARTS(3, "/img/cards/queen_of_hearts.png"),
    KING_OF_HEARTS(4, "/img/cards/king_of_hearts.png"),
    ACE_OF_HEARTS(11, "/img/cards/ace_of_hearts.png"),

    SIX_OF_SPADES(6, "/img/cards/6_of_spades.png"),
    SEVEN_OF_SPADES(7, "/img/cards/7_of_spades.png"),
    EIGHT_OF_SPADES(8, "/img/cards/8_of_spades.png"),
    NINE_OF_SPADES(9, "/img/cards/9_of_spades.png"),
    TEN_OF_SPADES(10, "/img/cards/10_of_spades.png"),
    JACK_OF_SPADES(2, "/img/cards/jack_of_spades.png"),
    QUEEN_OF_SPADES(3, "/img/cards/queen_of_spades.png"),
    KING_OF_SPADES(4, "/img/cards/king_of_spades.png"),
    ACE_OF_SPADES(11, "/img/cards/ace_of_spades.png");

    Integer points;
    String imageUrl;

    private static final List<CardEnum> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    CardEnum(Integer points, String imageUrl) {
        this.points = points;
        this.imageUrl = imageUrl;
    }

    public static CardEnum getRandomCard() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

    public Integer getPoints() {
        return points;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
