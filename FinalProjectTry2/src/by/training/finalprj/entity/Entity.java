package by.training.finalprj.entity;

import java.io.Serializable;

/**
 * An abstract entity which implements some common entity methods
 */
public abstract class Entity implements Serializable, Cloneable {

    private Integer id;

    public Entity() {
    }

    public Entity(Integer id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}