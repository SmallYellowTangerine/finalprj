package by.training.finalprj.entity;

import java.time.LocalDateTime;

/**
 * This is a message entity
 */
public class Message extends Entity implements Comparable<Message> {

    private String text;
    private String sender;
    private String receiver;
    private LocalDateTime sentDateTime;
    private boolean isRead;

    public Message() {
        super();
    }

    public Message(String sender, String receiver, LocalDateTime sentDateTime, boolean isRead, String text) {
        this.sender = sender;
        this.receiver = receiver;
        this.sentDateTime = sentDateTime;
        this.isRead = isRead;
        this.text = text;
    }

    public Message(Integer id, String sender, String receiver, LocalDateTime sentDateTime, boolean isRead, String text) {
        super(id);
        this.sender = sender;
        this.receiver = receiver;
        this.sentDateTime = sentDateTime;
        this.isRead = isRead;
        this.text = text;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public LocalDateTime getSentDateTime() {
        return sentDateTime;
    }

    public void setSentDateTime(LocalDateTime sentDateTime) {
        this.sentDateTime = sentDateTime;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int compareTo(Message o) {
        return getSentDateTime().compareTo(o.getSentDateTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        if (isRead() != message.isRead()) return false;
        if (getText() != null ? !getText().equals(message.getText()) : message.getText() != null) return false;
        if (getSender() != null ? !getSender().equals(message.getSender()) : message.getSender() != null) return false;
        if (getReceiver() != null ? !getReceiver().equals(message.getReceiver()) : message.getReceiver() != null)
            return false;
        return getSentDateTime().equals(message.getSentDateTime());
    }

    @Override
    public int hashCode() {
        int result = getText() != null ? getText().hashCode() : 0;
        result = 31 * result + (getSender() != null ? getSender().hashCode() : 0);
        result = 31 * result + (getReceiver() != null ? getReceiver().hashCode() : 0);
        result = 31 * result + getSentDateTime().hashCode();
        result = 31 * result + (isRead() ? 1 : 0);
        return result;
    }
}
