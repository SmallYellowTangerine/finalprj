package by.training.finalprj.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * This is a dialogue entity (doesn't map to a database table)
 */
public class Dialogue extends Entity {

    private String user;
    private String partner;
    private ArrayList<Message> messages;
    private LocalDateTime lastMessageDateTime;
    private int unreadMessages;

    public Dialogue() {
        super();
        messages = new ArrayList<>();
    }

    public Dialogue(String user, String partner, ArrayList<Message> messages, LocalDateTime lastMessageDateTime, int unreadMessages) {
        this.user = user;
        this.partner = partner;
        this.messages = messages;
        this.lastMessageDateTime = lastMessageDateTime;
        this.unreadMessages = unreadMessages;
    }

    public Dialogue(Integer id, String user, String partner, ArrayList<Message> messages, LocalDateTime lastMessageDateTime,
                    int unreadMessages) {
        super(id);
        this.user = user;
        this.partner = partner;
        this.messages = messages;
        this.lastMessageDateTime = lastMessageDateTime;
        this.unreadMessages = unreadMessages;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public LocalDateTime getLastMessageDateTime() {
        return lastMessageDateTime;
    }

    public void setLastMessageDateTime(LocalDateTime lastMessageDateTime) {
        this.lastMessageDateTime = lastMessageDateTime;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dialogue)) return false;
        Dialogue dialogue = (Dialogue) o;
        if (getUnreadMessages() != dialogue.getUnreadMessages()) return false;
        if (!getUser().equals(dialogue.getUser())) return false;
        if (!getPartner().equals(dialogue.getPartner())) return false;
        if (getMessages() != null ? !getMessages().equals(dialogue.getMessages()) : dialogue.getMessages() != null)
            return false;
        return getLastMessageDateTime() != null ? getLastMessageDateTime().equals(dialogue.getLastMessageDateTime()) : dialogue.getLastMessageDateTime() == null;
    }

    @Override
    public int hashCode() {
        int result = getUser().hashCode();
        result = 31 * result + getPartner().hashCode();
        result = 31 * result + (getMessages() != null ? getMessages().hashCode() : 0);
        result = 31 * result + (getLastMessageDateTime() != null ? getLastMessageDateTime().hashCode() : 0);
        result = 31 * result + getUnreadMessages();
        return result;
    }
}
