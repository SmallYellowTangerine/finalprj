package by.training.finalprj.entity;

/**
 * This is a gameuser entity
 */
public class GameUser extends Entity {

    private Integer gameId;
    private Integer playerId;

    public GameUser() {
    }

    public GameUser(Integer id, Integer gameId, Integer playerId) {
        super(id);
        this.gameId = gameId;
        this.playerId = playerId;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameUser)) return false;
        GameUser that = (GameUser) o;
        if (getGameId() != null ? !getGameId().equals(that.getGameId()) : that.getGameId() != null) return false;
        return getPlayerId() != null ? getPlayerId().equals(that.getPlayerId()) : that.getPlayerId() == null;
    }

    @Override
    public int hashCode() {
        int result = getGameId() != null ? getGameId().hashCode() : 0;
        result = 31 * result + (getPlayerId() != null ? getPlayerId().hashCode() : 0);
        return result;
    }
}
