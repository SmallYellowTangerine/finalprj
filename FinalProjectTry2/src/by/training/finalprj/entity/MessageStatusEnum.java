package by.training.finalprj.entity;

/**
 * This is an enum of possible messaging users' states
 */
public enum MessageStatusEnum {
    ONLINE,
    OFFLINE,
    STARTING,
    FINISHING,
    SENDING;
}
