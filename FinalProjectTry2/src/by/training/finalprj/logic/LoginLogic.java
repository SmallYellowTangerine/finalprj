package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This is a business logic class which provides functionality for user
 * authentication
 */
public class LoginLogic {

    public boolean checkLogin(String enterLogin, String enterPassword) throws LogicException {
        String encryptedPassword = PasswordEncoder.encrypt(enterPassword);
        UserDAO userDao = new UserDAO();
        User user;
        try {
            user = userDao.findUserByNameAndPassword(enterLogin, encryptedPassword);
        } catch (DAOException e) {
            throw new LogicException("Error finding user by name and password", e);
        }
        return (user != null);
    }

    public boolean checkAdmin(String login) throws LogicException {
        UserDAO userDao = new UserDAO();
        Boolean isAdmin;
        try {
            User user = userDao.findUserByName(login);
            isAdmin = (user.getRole()== 2);
        } catch (DAOException e) {
            throw new LogicException("Error finding user by name and password", e);
        }
        return isAdmin;
    }

    public boolean checkBlocked(String login) throws LogicException {
        UserDAO userDao = new UserDAO();
        Boolean isBlocked;
        try {
            User user = userDao.findUserByName(login);
            isBlocked = (user.getRole()== 3);
        } catch (DAOException e) {
            throw new LogicException("Error finding user by name and password", e);
        }
        return isBlocked;
    }

    public void sortAllUsersByRating(ArrayList<User> allUsers) {
        allUsers.sort(new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o2.getRating().compareTo(o1.getRating());
            }
        });
    }
}
