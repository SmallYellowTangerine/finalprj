package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.TransactionDAO;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;

import java.math.BigDecimal;

/**
 * This is a business logic class which provides functionality for betting
 * in user vs user (PvP) type games
 */
public class PvpBetLogic {

    public User makeBet(String username, BigDecimal sum) throws LogicException {
        //get user object
        User user = PvpGameManager.getPlayer(username);
        if (user == null) {
            throw new LogicException("Can't get player while making bet");
        }
        //check if it's less than user's balance
        if (user.getBalance().compareTo(sum) < 0) {
            return null;
        }
        //remove money
        BigDecimal newAccountValue = user.getBalance().subtract(sum);
        user.setBalance(newAccountValue);
        //set bet amount
        user.setCurrentBet(sum);
        //remove money in db too
        boolean transactionInserted;
        try {
            UserDAO userDao = new UserDAO();
            userDao.updateBalance(user, newAccountValue);
            //transaction to DB
            TransactionDAO transactionDAO = new TransactionDAO();
            transactionInserted = transactionDAO.insertTransaction(user, sum.multiply(BigDecimal.valueOf(-1)));
            if (!transactionInserted) {
                throw new LogicException("Couldn't record transaction in dao");
            }
            return user;
        } catch (DAOException e) {
            throw new LogicException("Couldn't record bet in dao while making bet", e);
        }
    }

    void transferMoney(User winner, BigDecimal sum) throws LogicException {
        //add money
        BigDecimal newBalanceValue = winner.getBalance().add(sum);
        winner.setBalance(newBalanceValue);
        //add money in db too
        try {
            UserDAO userDao = new UserDAO();
            userDao.updateBalance(winner, newBalanceValue);
        } catch (DAOException e) {
            throw new LogicException("Couldn't update balance in dao while transfering money after the game", e);
        }
    }
}
