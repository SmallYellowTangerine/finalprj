package by.training.finalprj.logic;

import by.training.finalprj.dao.*;
import by.training.finalprj.entity.*;
import by.training.finalprj.manager.ResultMessageEnum;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import static by.training.finalprj.entity.Game.GameStatus.FINISHED_CORRECTLY;
import static by.training.finalprj.entity.GamingStatusEnum.*;

/**
 * This is a business logic class which provides functionality for playing
 * user vs AI(server) type games)
 */
public class AiGameLogic {

    public static ConcurrentHashMap<String, ArrayList<CardEnum>> activeGamesCardLists = new ConcurrentHashMap<>();

    public CardEnum getRandomCard(User user) throws LogicException {
        CardEnum card;
        //write drawn cards into currentgame
        do {
            //get a random card
            card = CardEnum.getRandomCard();
        } while (!recordCard(user, card));
        user.getDrawnCards().add(card);
        user.setCurrentPoints(user.getCurrentPoints() + card.getPoints());
        //determine if player lost or won or game continues
        determineStatus(user);
        return card;
    }

    private void determineStatus(User user) throws LogicException {
        if (user.getCurrentPoints() == 21) {
            user.setGamingStatus(AI_GAME_USER_WON);
        } else if (user.getCurrentPoints() > 21) {
            user.setGamingStatus(AI_GAME_USER_LOST);
        } else {
            user.setGamingStatus(AI_PLAYER_CONTINUES);
        }
        //if the game is over
        if (user.getGamingStatus() != AI_PLAYER_CONTINUES) {
            processGameEnd(user);
        }
    }

    private boolean recordCard(User user, CardEnum card) {
        for (CardEnum existingCard : user.getDrawnCards()) {
            if (existingCard.equals(card)) {
                return false;
            }
        }
        return true;
    }

    private void processGameEnd(User user)
            throws LogicException {
        Game currentGame = new Game();
        user.setCurrentGame(currentGame);
        //record changes to DB
        user.getCurrentGame().setGameStatus(FINISHED_CORRECTLY);
        user.getCurrentGame().setWinner(user.getGamingStatus() == AI_GAME_USER_LOST
                ? null
                : user);
        UserDAO userDao = new UserDAO();
        try {
            GameDAO gameDAO = new GameDAO();
            UserGameDAO userGameDAO = new UserGameDAO();
            //update rating
            userDao.updateRating(user);
            //update games won and lost
            userDao.updateGamesWonLost(user);
            //record game and playergame
            gameDAO.insertGame(user.getCurrentGame(), user.getId());
            GameUser gameUser = new GameUser();
            gameUser.setGameId(user.getCurrentGame().getId());
            gameUser.setPlayerId(user.getId());
            userGameDAO.insertOrFindPlayerGame(user, user.getCurrentGame());
        } catch (DAOException e) {
            throw new LogicException("Couldn't update game in dao when game ended", e);
        }
    }
    
    public String aiDrawCards(User user) {
        //draw cards until we get more than user's points
        ArrayList<CardEnum> aiDrawnCards = new ArrayList<>();
        int points = 0;
        CardEnum card;
        do {
            //get a random card
            card = CardEnum.getRandomCard();
            if (!aiDrawnCards.contains(card)) {
                aiDrawnCards.add(card);
                points += card.getPoints();
            }
        } while (points <= user.getCurrentPoints());
        activeGamesCardLists.put(user.getLogin(), aiDrawnCards);
        //determine if computer wins or loses
        if (points > 21) {
            user.setGamingStatus(AI_GAME_USER_WON);
        } else {
            user.setGamingStatus(AI_GAME_USER_LOST);
        }
        //send result message
        return ResultMessageEnum.AI_GAME_OVER.getName();
    }

    public void endGame(User user) {
        user.setCurrentPoints(null);
        user.setGamingStatus(null);
        user.setCurrentGame(null);
        user.setDrawnCards(new ArrayList<>());
        activeGamesCardLists.remove(user.getLogin());
    }
}
