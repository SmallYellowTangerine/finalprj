package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;

/**
 * This is a business logic class which provides functionality for deleting
 * a user (by administrator)
 */
public class DeleteUserLogic {

        public boolean deleteUser(String username) throws LogicException {
            UserDAO userDAO = new UserDAO();
            try {
            return userDAO.deleteUser(username);
        } catch (DAOException e) {
            throw new LogicException("Error in deleting");
        }
    }
}
