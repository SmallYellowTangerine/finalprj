package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;

import java.util.ArrayList;

/**
 * This is a business logic class which provides functionality for showing
 * a list of user info fro administrators
 */
public class DisplayUsersLogic {

    public ArrayList<User> getAllUsers() throws LogicException {
        UserDAO userDAO = new UserDAO();
        try {
            return userDAO.getAllUsers();
        } catch (DAOException e) {
            throw new LogicException("Couldn't get all users via dao", e);
        }
    }
}
