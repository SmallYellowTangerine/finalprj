package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.MessageDAO;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.Dialogue;
import by.training.finalprj.entity.Message;
import by.training.finalprj.entity.User;

import java.util.*;

/**
 * This is a business logic class which provides functionality for fetching
 * dialogues (lists of messages) from database
 */
public class DialogueLogic {

    public SortedSet<Dialogue> getDialogues(String username) throws LogicException {
        Map<String, Dialogue> allDialogues = new HashMap<>();
        UserDAO userDAO = new UserDAO();
        MessageDAO messageDAO = new MessageDAO();
        try {
            User user = userDAO.findUserByName(username);
            ArrayList<Message> allMessages = messageDAO.findMessagesByUser(user.getId());
            sortMessagesToDialogues(username, allDialogues, allMessages);
            for (Dialogue currentDialogue : allDialogues.values()) {
                sortMessagesInDialogueByDate(currentDialogue);
            }
            //sort dialogues by dates
            SortedSet<Dialogue> sortedDialogues = new TreeSet<>(
                    new Comparator<Dialogue>() {
                        @Override
                        public int compare(Dialogue dialogue1, Dialogue dialogue2) {
                            return dialogue2.getLastMessageDateTime().compareTo(dialogue1.getLastMessageDateTime());
                        }
                    });
            sortedDialogues.addAll(allDialogues.values());
            return sortedDialogues;
        } catch (DAOException e) {
            throw new LogicException("Something wrong while getting dialogues via dao", e);
        }
    }

    private void sortMessagesInDialogueByDate(Dialogue dialogue) {
        Collections.sort(dialogue.getMessages());
        dialogue.setLastMessageDateTime(dialogue.getMessages().get(dialogue.getMessages().size()-1)
                .getSentDateTime());
    }

    private void sortMessagesToDialogues(String username, Map<String, Dialogue> allDialogues, ArrayList<Message> allMessages) {
        String partner;
        Dialogue dialogue;
        for (Message message : allMessages) {
            partner = message.getSender().equals(username) ? message.getReceiver() : message.getSender();
            if ( !(allDialogues.containsKey(message.getReceiver())
                    || allDialogues.containsKey(message.getSender()) ) ) {
                dialogue = new Dialogue();
                dialogue.getMessages().add(message);
                dialogue.setUser(username);
                dialogue.setPartner(partner);
                if (!message.isRead()) {
                    dialogue.setUnreadMessages(dialogue.getUnreadMessages() + 1);
                }
                allDialogues.put(partner, dialogue);
            } else {
                dialogue = allDialogues.get(partner);
                dialogue.getMessages().add(message);
                if (!message.isRead()) {
                    dialogue.setUnreadMessages(dialogue.getUnreadMessages() + 1);
                }
            }
        }
    }

    public Dialogue searchDialogue(String username, String partnerUsername) throws LogicException {
        User foundUser;
        Dialogue dialogue;
        UserDAO userDAO = new UserDAO();
        MessageDAO messageDAO = new MessageDAO();
        try {
            foundUser = userDAO.findUserByName(partnerUsername);
            dialogue = new Dialogue();
            if (foundUser != null) {
                dialogue.setUser(username);
                dialogue.setPartner(partnerUsername);
                User user = userDAO.findUserByName(username);
                ArrayList<Message> allMessages = messageDAO.findMessagesByTwoUsers(user.getId(), foundUser.getId());
                for (Message message : allMessages) {
                    dialogue.getMessages().add(message);
                    if (!message.isRead()) {
                        dialogue.setUnreadMessages(dialogue.getUnreadMessages() + 1);
                    }
                }
                //sort messages inside dialogue by dates
                if (allMessages.size() != 0) {
                    sortMessagesInDialogueByDate(dialogue);
                }
            }
            return dialogue;
        } catch (DAOException e) {
            throw new LogicException("Something wrong while searching forn dialogue via dao", e);
        }
    }

    public SortedSet<Dialogue> getAdminDialogues(String username) throws LogicException {
        Dialogue dialogue;
        Map<String, Dialogue> allDialogues = new HashMap<>();
        UserDAO userDAO = new UserDAO();
        try {
            ArrayList<User> admins = userDAO.getAllUsersWithRole(2);
            for (User admin : admins) {
                dialogue = searchDialogue(username, admin.getLogin());
                allDialogues.put(admin.getLogin(), dialogue);
            }
            //sort dialogues by dates
            SortedSet<Dialogue> sortedDialogues = new TreeSet<>(
                    new Comparator<Dialogue>() {
                        @Override
                        public int compare(Dialogue dialogue1, Dialogue dialogue2) {
                            int result;
                            if (dialogue1.getLastMessageDateTime() == null || dialogue2.getLastMessageDateTime() == null) {
                                return 0;
                            }
                            result = dialogue2.getLastMessageDateTime().compareTo(dialogue1.getLastMessageDateTime());
                            return result;
                        }
                    });
            sortedDialogues.addAll(allDialogues.values());
            return sortedDialogues;
        } catch (DAOException e) {
            throw new LogicException("Something wrong while getting admin dialogues via dao", e);
        }
    }
}
