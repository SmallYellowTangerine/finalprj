package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is a business logic class which provides functionality for blocking
 * (banning) a user (by administrator)
 */
public class BlockLogic {

    private final Logger LOGGER = LogManager.getRootLogger();

    public Boolean blockUser(String username) throws LogicException {
        boolean result;
        UserDAO userDAO = new UserDAO();
        try {
            User user = userDAO.findUserByName(username);
            int newRole = user.getRole() == 1 ? 3 : 1;
            result = userDAO.updateRole(username, newRole);
            if (result) {
                LOGGER.info("Blocked user with id " + user.getId());
            }
            return result;
        } catch (DAOException e) {
            throw new LogicException("Error in Block");
        }
    }
}
