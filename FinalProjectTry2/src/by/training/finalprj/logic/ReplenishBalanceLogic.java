package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;

/**
 * This is a business logic class which provides functionality for putting more "money"
 * in their account
 */
public class ReplenishBalanceLogic {

    private final Logger LOGGER = LogManager.getRootLogger();

    public Boolean replenishBalance(String username, String password, String sumString) throws LogicException {
        String encryptedPassword = PasswordEncoder.encrypt(password);
        boolean result;
        UserDAO userDAO = new UserDAO();
        UserDAO userDao = new UserDAO();
        try {
            User user = userDAO.findUserByNameAndPassword(username, encryptedPassword);
            if (user == null) {
                return  false;
            }
            //check is the sum is alright
            BigDecimal sum = new BigDecimal(sumString);
            if (sum.compareTo(new BigDecimal("0")) <= 0) {
                throw new LogicException("The sum is less or equals zero");
            }
            //through dao - put sum on the account
            BigDecimal updatedBalance = user.getBalance().add(sum);
            result = userDao.updateBalance(user, updatedBalance);
            if (result) {
                LOGGER.info("User " + user.getId() + " updated balance by " + sum);
            }
            return result;
        } catch (DAOException e) {
            throw new LogicException("Couldn't replenish balance due to dao", e);
        }
    }

}
