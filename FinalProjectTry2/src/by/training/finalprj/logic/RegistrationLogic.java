package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;

/**
 * This is a business logic class which provides functionality for users' authorization
 */
public class RegistrationLogic {

    private final Logger LOGGER = LogManager.getRootLogger();

    public boolean registerUser(String login, String password, String email, String cardNumber) throws LogicException {
        String encryptedPassword = PasswordEncoder.encrypt(password);
        Boolean userInserted;
        try {
            if ( !(validateLogin(login) &&
                validatePassword(password) &&
                validateEmail(email) &&
                validateCard(cardNumber)) ) {
                return false;
            }
            UserDAO userDao = new UserDAO();
            if (userDao.findUserByName(login) != null) {
                return false;
            }
            User newUser = new User(login, encryptedPassword, email, LocalDate.now(), 1);
            newUser.setBankCardNumber(cardNumber);
            userInserted = userDao.insertUser(newUser);
            if (userInserted) {
                LOGGER.info("Registered user with login: " + newUser.getLogin() + ", id: " + newUser.getId());
            }
        } catch (DAOException e) {
            throw new LogicException("Couldn't in dao while registering user", e);
        }
        return userInserted;
    }

    public boolean validateLogin(String login) {
        String regex = "^[a-zA-Z][a-zA-Z0-9_]{4,}$";
        return login.matches(regex);
    }

    public boolean validatePassword(String password) {
        String regex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).+$";
        return password.matches(regex);
    }

    public boolean validateEmail(String email) {
        return (email.contains(".") && email.contains("@"));
    }

    public boolean validateCard(String cardNumber) {
        String regex = "^[0-9]{16}$";
        return cardNumber.matches(regex);
    }
}
