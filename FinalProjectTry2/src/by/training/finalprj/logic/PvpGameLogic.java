package by.training.finalprj.logic;

import by.training.finalprj.dao.*;
import by.training.finalprj.entity.*;

import java.math.BigDecimal;

import static by.training.finalprj.entity.Game.GameStatus.FINISHED_CORRECTLY;
import static by.training.finalprj.entity.Game.GameStatus.UNFINISHED;
import static by.training.finalprj.entity.GamingStatusEnum.*;

/**
 * This is a business logic class which provides functionality for playing
 * user vs user type games
 */
public class PvpGameLogic {

    public CardEnum getRandomCard(String username) throws LogicException {
        CardEnum card;
        User player = PvpGameManager.getPlayer(username);
        //write drawn cards into currentgame
        do {
            //get a random card
            card = CardEnum.getRandomCard();
        } while (!PvpGameManager.recordCard(player, card));
        player.getDrawnCards().add(card);
        player.setCurrentPoints(player.getCurrentPoints() + card.getPoints());
        //determine if player lost or won or game continues
        determineStatus(player, username);
        return card;
    }

    private void determineStatus(User player, String username) throws LogicException {
        User partner = PvpGameManager.getPartner(player.getCurrentGame(), username);
        BigDecimal sum = player.getCurrentBet().add(partner.getCurrentBet());
        PvpBetLogic pvpBetLogic = new PvpBetLogic();
        if (player.getCurrentPoints() == 21) {
            player.setGamingStatus(PLAYER_WON);
            partner.setGamingStatus(PLAYER_LOST);
            pvpBetLogic.transferMoney(player, sum);
        } else if (player.getCurrentPoints() > 21) {
            player.setGamingStatus(PLAYER_LOST);
            partner.setGamingStatus(PLAYER_WON);
            //transwer money to the winner
            pvpBetLogic.transferMoney(partner, sum);
        } else {
            if (partner.getGamingStatus() == PASSED) {
                if (player.getCurrentPoints() > partner.getCurrentPoints()) {
                    player.setGamingStatus(PLAYER_WON);
                    //transwer money to the winner
                    pvpBetLogic.transferMoney(player, sum);
                }
            } else {
                player.setGamingStatus(PLAYER_CONTINUES);
            }
        }
        //if the game is over
        if (player.getGamingStatus() != PLAYER_CONTINUES) {
            processGameEnd(player, username, partner, sum);
        }
    }

    private void processGameEnd(User playerProfile, String username, User partner, BigDecimal amount)
            throws LogicException {
        //record changes to DB
        playerProfile.getCurrentGame().setGameStatus(FINISHED_CORRECTLY);
        playerProfile.getCurrentGame().setWinner(playerProfile.getGamingStatus() == PLAYER_LOST
                                            ? PvpGameManager.getPartner(playerProfile.getCurrentGame(), username)
                                            : PvpGameManager.getPlayer(username) );
        UserDAO userDao = new UserDAO();
        try {
            //update rating
            userDao.updateRating(playerProfile);
            userDao.updateRating(partner);
            //update games won and lost
            userDao.updateGamesWonLost(playerProfile);
            userDao.updateGamesWonLost(partner);
            GameDAO gameDAO = new GameDAO();
            gameDAO.finalUpdateGame(playerProfile.getCurrentGame());
            TransactionDAO transactionDAO = new TransactionDAO();
            transactionDAO.insertTransaction(PvpGameManager.getPlayer(username), amount);
        } catch (DAOException e) {
            throw new LogicException("Couldn't update game in dao when game ended", e);
        }
    }

    public void endGameEarly(User player, User partner) throws LogicException {
        //set gaming statuses right (if the game ended suddenly, delete player's, set partner's to partner_left)
        player.setGamingStatus(null);
        if (partner != null) {
            partner.setGamingStatus(PARTNER_LEFT);
        }
        TransactionDAO transactionDAO = new TransactionDAO();
        boolean transactionInserted;
        try {
            //Return money to both
            returnMoneyToPlayer(player, transactionDAO);
            returnMoneyToPartner(partner, transactionDAO);
            //Delete game from map of games
            Game game = player.getCurrentGame();
            PvpGameManager.getAllGames().remove(game);
            //set game status in db to unfinished
            game.setGameStatus(UNFINISHED);
            GameDAO gameDAO = new GameDAO();
            gameDAO.finalUpdateGame(game);
        } catch (DAOException e) {
            throw new LogicException("DAO exception");
        }
    }

    private void returnMoneyToPartner(User partner, TransactionDAO transactionDAO) throws DAOException, LogicException {
        boolean transactionInserted;
        if (partner != null && partner.getCurrentBet() != null) {
            BigDecimal partnerBetReturn = partner.getCurrentBet().negate();
            transactionInserted = transactionDAO.insertTransaction(partner, partnerBetReturn);
            if (!transactionInserted) {
                throw new LogicException("Couldn't record transaction to user in dao");
            }
            partner.setBalance(partner.getBalance().subtract(partnerBetReturn));
        }
    }

    private void returnMoneyToPlayer(User player, TransactionDAO transactionDAO) throws DAOException, LogicException {
        boolean transactionInserted;
        if (player.getCurrentBet() != null) {
            BigDecimal playerBetReturn = player.getCurrentBet().negate();
            transactionInserted = transactionDAO.insertTransaction(player, playerBetReturn);
            if (!transactionInserted) {
                throw new LogicException("Couldn't record transaction to user in dao");
            }
            player.setBalance(player.getBalance().subtract(playerBetReturn));
        }
    }

}
