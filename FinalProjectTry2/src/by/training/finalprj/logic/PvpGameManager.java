package by.training.finalprj.logic;

import by.training.finalprj.entity.CardEnum;
import by.training.finalprj.entity.Game;
import by.training.finalprj.entity.User;

import java.util.*;

/**
 * This is a business logic class which provides functionality for managing
 * active user vs user (PvP) type games
 */
public class PvpGameManager {

    private static List<Game> allGames = Collections.synchronizedList( new ArrayList<Game>());

    public static User getPlayer(String username) {
        for (Game game : allGames) {
            for (User player : game.getPlayers().values()) {
                if (player.getLogin().equals(username)) {
                    return player;
                }
            }
        }
        return null;
    }

    public static User getPartner(Game game, String username) {
        for (User player : game.getPlayers().values()) {
            if (!player.getLogin().equals(username)) {
                return player;
            }
        }
        return null;
    }

    public static Boolean recordCard(User player, CardEnum card) {
        //check this player's cards
        for (CardEnum existingCard : player.getDrawnCards()) {
            if (existingCard.equals(card)) {
                return false;
            }
        }
        //check their partner's cards
        for (CardEnum existingCard : getPartner(player.getCurrentGame(), player.getLogin()).getDrawnCards()) {
            if (existingCard.equals(card)) {
                return false;
            }
        }
        return true;
    }

    public static List<Game> getAllGames() {
        return allGames;
    }

    public static void setAllGames(List<Game> allGames) {
        PvpGameManager.allGames = allGames;
    }
}
