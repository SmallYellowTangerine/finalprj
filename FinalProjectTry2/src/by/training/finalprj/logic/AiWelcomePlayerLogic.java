package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.User;

/**
 * This is a business logic class which provides functionality for welcoming player
 * during user vs AI(server) type games
 */
public class AiWelcomePlayerLogic {

    public User registerPlayer(String username) throws LogicException {
        UserDAO userDAO = new UserDAO();
        try {
            return userDAO.findUserByName(username);
        } catch (DAOException e) {
            throw new LogicException("Couldn't find user in dao while welcoming player", e);
        }
    }
}
