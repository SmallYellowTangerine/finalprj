package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.GameDAO;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.Game;
import by.training.finalprj.entity.GamingStatusEnum;
import by.training.finalprj.entity.User;

/**
 * This is a business logic class which provides functionality for registering
 * players in user vs user (PvP) type games
 */
public class PvpRegisterPlayerLogic {

    public User registerPlayer(String username) throws LogicException {
        try {
            UserDAO userDAO = new UserDAO();
            User currentPlayer = userDAO.findUserByName(username);
            //check if there are existing games that wait for player two
            User partner = checkForWaitingGames(currentPlayer);
            //if there are no existing games
            Boolean newGameCreated;
            if (partner == null) {
                newGameCreated = createNewGame(currentPlayer);
                currentPlayer.setGamingStatus(GamingStatusEnum.REGISTERED_FIRST);
                if (!newGameCreated) {
                    throw new LogicException("Error while inserting new game to dao");
                }
            } else {
                partner.setGamingStatus(GamingStatusEnum.MAKING_BET);
                partner.setCurrentGame(currentPlayer.getCurrentGame());
                currentPlayer.setGamingStatus(GamingStatusEnum.MAKING_BET);
            }
            return currentPlayer;
        } catch (DAOException e) {
            throw new LogicException("Couldn't find user in dao while registering player", e);
        }
    }

    private boolean createNewGame(User currentPlayer) throws DAOException {
        Boolean result;
        Game currentGame = new Game();
        GameDAO gameDAO = new GameDAO();
        currentGame.getPlayers().put(currentPlayer.getLogin(), currentPlayer);
        currentPlayer.setCurrentGame(currentGame);
        currentGame.setGameStatus(Game.GameStatus.CREATED);
        result = gameDAO.insertGame(currentGame, null);
        if (result) {
            PvpGameManager.getAllGames().add(currentGame);
        }
        return result;
    }

    private User checkForWaitingGames(User currentPlayer) throws DAOException {
        User partner = null;
        GameDAO gameDAO = new GameDAO();
        for (Game existingGame : PvpGameManager.getAllGames()) {
            if (existingGame.getPlayers().size() < 2) {
                existingGame.getPlayers().put(currentPlayer.getLogin(), currentPlayer);
                partner = PvpGameManager.getPartner(existingGame, currentPlayer.getLogin());
                currentPlayer.setCurrentGame(existingGame);
                existingGame.setGameStatus(Game.GameStatus.STARTED);
                gameDAO.updateGame(existingGame);
                break;
            }
        }
        return partner;
    }
}
