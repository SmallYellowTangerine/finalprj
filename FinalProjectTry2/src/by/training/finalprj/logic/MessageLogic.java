package by.training.finalprj.logic;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.dao.MessageDAO;
import by.training.finalprj.dao.UserDAO;
import by.training.finalprj.entity.Message;
import by.training.finalprj.entity.MessageStatusEnum;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Администратор on 19.07.2017.
 */
public class MessageLogic {

    private static ConcurrentHashMap<User, String> onlineInterlocutors = new ConcurrentHashMap<>();
    private static final String MESSAGE_ROW_WRAPPER = "<tr>\n" +
            "<td>%s</td>\n" +
            "<td>%s</td>\n" +
            "<td>%s</td>\n" +
            "</tr>";

    public ArrayList<Message> getAllMessages(String username, String partnerUsername) throws LogicException {
        //when this logic gets called all messages unread by this user (=sent by partner) become read
        ArrayList<Message> messages;
        UserDAO userDAO = new UserDAO();
        MessageDAO messageDAO = new MessageDAO();
        try {
            User user = userDAO.findUserByName(username);
            User partner = userDAO.findUserByName(partnerUsername);
            messages = messageDAO.findMessagesByTwoUsers(user.getId(), partner.getId());
            Collections.sort(messages);
            Collections.reverse(messages);
            return messages;
        } catch (DAOException e) {
            throw new LogicException("Couldn't get messages via dao", e);
        }
    }

    public String sendMessage(String username, String partnerUsername, String inputMessage, MessageStatusEnum partnerStatus)
            throws LogicException {
        String message;
        boolean isRead;
        MessageDAO messageDAO = new MessageDAO();
        boolean messageInserted;
        try {
            isRead = (partnerStatus == MessageStatusEnum.ONLINE);
            //save message to database
            Message newMessage = new Message(username, partnerUsername, LocalDateTime.now(), isRead, inputMessage);
            messageInserted = messageDAO.insertMessage(newMessage);
            if (!messageInserted) {
                throw new LogicException("Couldn't insert message in dao");
            }
            //wrap it in appropriate wrapping of table row
            message = String.format(MESSAGE_ROW_WRAPPER, username, newMessage.getSentDateTime(), inputMessage);
        } catch (DAOException e) {
            throw new LogicException("Couldn't send message", e);
        }
        return message;
    }

    public MessageStatusEnum checkPartnerStatus(String username, String partnerUsername) throws LogicException {
        MessageStatusEnum partnerStatus = MessageStatusEnum.OFFLINE;
        UserDAO userDAO = new UserDAO();
        try {
            //if user's not in collection, add them
            User currentUser = userDAO.findUserByName(username);
            onlineInterlocutors.putIfAbsent(currentUser, partnerUsername);
            //check if their partner is online
            User partner = userDAO.findUserByName(partnerUsername);
            if (onlineInterlocutors.containsKey(partner) && onlineInterlocutors.get(partner).equals(username)) {
                partnerStatus = MessageStatusEnum.ONLINE;
            }
        } catch (DAOException e) {
            throw new LogicException("Couldn't find user by name via dao while checking partner status", e);
        }
        return partnerStatus;
    }

    public void removeInterlocutor(String username) throws LogicException {
        UserDAO userDAO = new UserDAO();
        try {
            onlineInterlocutors.remove(userDAO.findUserByName(username));
        } catch (DAOException e) {
            throw new LogicException("Couldn't find user by name via dao while removing them from a collection of online interlocutors", e);
        }
    }

    public void setReadMessages(String partnerUsername, ArrayList<Message> allMessages) throws LogicException {
        MessageDAO messageDAO = new MessageDAO();
        boolean messageReadUpdated;
        try {
            for (Message message : allMessages) {
                if (!message.isRead() && message.getSender().equals(partnerUsername)) {
                    message.setRead(true);
                    messageReadUpdated = messageDAO.updateMessageRead(message);
                    if (!messageReadUpdated) {
                        throw new LogicException("Couldn't update message's read property via dao");
                    }
                }
            }
        } catch (DAOException e) {
            throw new LogicException("Couldn't update message's read property via dao", e);
        }
    }
}
