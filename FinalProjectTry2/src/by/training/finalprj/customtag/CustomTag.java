package by.training.finalprj.customtag;

import by.training.finalprj.manager.ParameterNameEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * This is a custom jstl tag which displays user's name to them on the main page
 */
public class CustomTag extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {
        HttpServletRequest request = (HttpServletRequest)getJspContext().getAttribute("javax.servlet.jsp.jspRequest");
        if (request.getSession() != null) {
            HttpSession session = request.getSession();
            if (session.getAttribute(ParameterNameEnum.USER.getName()) != null) {
                getJspContext().getOut().write((String) session.getAttribute(ParameterNameEnum.USER.getName()));
            }
        }
    }

}
