package by.training.finalprj.manager;

/**
 * Enum of all possible string parameter names, keys etc
 */
public enum ParameterNameEnum {
    COMMAND("command"),
    WRONG_ACTION("wrongAction"),
    USER("user"),
    MESSAGING("messaging"),
    GAME_OVER("gameOver"),
    HTTPSESSION("javax.servlet.http.HttpSession"),
    INPUT_MESSAGE("inputMessage"),
    PARTNER("partner"),
    PARTNER_USERNAME("partnerUsername"),
    PARTNER_STATUS("partnerStatus"),
    USERNAME("username"),
    PASS("pass"),
    USER_LOGIN("userLogin"),
    OPERATION_RESULT("operationResult"),
    ERROR_OPERATION_MESSAGE("errorOperationMessage"),
    DIALOGUES("dialogues"),
    MESSAGES("messages"),
    USERS("users"),
    LOCALE("lcl"),
    RU("ru"),
    EN_US("en_US"),
    RU_RU("ru_RU"),
    LOGIN("login"),
    PASSWORD("password"),
    LOGGED_IN("loggedIn"),
    USER_TYPE("userType"),
    ERROR_LOGIN_PASSWORD_MESSAGE("errorLoginPassMessage"),
    SUM("sum"),
    EMAIL("email"),
    CARD("card"),
    SEARCH_LOGIN("searchLogin"),
    NULL_PAGE("nullPage"),
    DB_URL("db.url"),
    DB_PASSWORD("db.password"),
    DB_USERNAME("db.username"),
    DB_POOL_SIZE("db.poolsize"),
    LOCALIZE("localize"),
    REGISTER("register"),
    PATHS("paths"),
    DATABASE("database"),
    ERRORMESSAGES("errormessages"),
    STATISTICS("statistics"),
    //WsPlayerResponse
    WSPLAYER("wsplayer"),
    REGISTERED_SECOND("registeredSecond"),
    DISPLAY_POINTS("displayPoints"),
    OPEN_CARD_IMAGE_TAG("openCardImage"),
    CLOSE_CARD_IMAGE_TAG("closeCardImage"),
    PLAYER_LOST("playerLost"),
    PLAYER_WON("playerWon"),
    PARTNER_POINTS("partnerPoints"),
    MAIN_PAGE_BUTTON("mainPageButton"),
    LOST_MONEY("lostMoney"),
    RATING("rating"),
    PASSED("passed"),
    CARD_BACK("cardBack"),
    DISPLAY_CARDS_BUTTON("displayCardsButton"),
    DISPLAY_PARTNER_POINTS("displayPartnerPoints"),
    DISPLAY_PASS_BUTTON("displayPassButton"),
    DISPLAY_OWN_POINTS("displayOwnPoints"),
    PARTNER_DREW("partnerDrew"),
    CLOSE_PARTNER_DREW("closePartnerDrew"),
    GAINED_MONEY("gainedMoney"),
    BR("<br/>"),
    //WsPartnerResponse
    WSPARTNER("wspartner"),
    CLOSE_DISPLAY_POINTS("closeDisplayPoints"),
    PLAYER_POINTS("playerPoints"),
    PLAYER_CARDS("playerCards"),
    //websocketcontroller
    PLAYER_LEFT_EARLY("playerLeftEarly"),
    PLAYER_LEFT("playerLeft"),
    //webscoketai
    MESSAGE("message"),
    GAME_VS_AI_START("gameVsAiStart"),
    AI_GAME("aigame"),
    AI_GAME_OVER("AiGameOver"),
    PARTNER_PASSED("PartnerPassed");

    private String name;

    ParameterNameEnum(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
