package by.training.finalprj.manager;

import java.util.ResourceBundle;

/**
 * Util class for fetching database properties from properties file
 */
public class DatabaseManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(ParameterNameEnum.DATABASE.getName());

    private DatabaseManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
