package by.training.finalprj.manager;

import java.util.ResourceBundle;

/**
 * Util class for fetching path names from properties file
 */
public class PathConfigurationManager {

    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(ParameterNameEnum.PATHS.getName());

    private PathConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
