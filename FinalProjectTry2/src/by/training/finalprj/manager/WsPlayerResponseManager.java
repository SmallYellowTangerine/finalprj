package by.training.finalprj.manager;

import by.training.finalprj.entity.CardEnum;
import by.training.finalprj.entity.GamingStatusEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.PvpGameManager;

import javax.websocket.Session;
import java.math.BigDecimal;
import java.util.ResourceBundle;

/**
 * Util class for managing response to current player in pvp game logic
 */
public class WsPlayerResponseManager {

    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(ParameterNameEnum.WSPLAYER.getName());

    private WsPlayerResponseManager() { }

    public static String getProperty(String key, Session session) {
        String message = "";
        if ( key.equals(ParameterNameEnum.REGISTERED_SECOND.getName()) ) {
            message = setRegisteredSecondResponse(session, message);
        }
        message += resourceBundle.getString(key);
        //player continues as usual
        if ( key.equals(ParameterNameEnum.DISPLAY_POINTS.getName()) ){
            message = setDisplayPointsResponse(session, message);
        }
        if ( key.equals(ParameterNameEnum.GAME_OVER.getName()) ) {
            message = setGameOverResponse(session, message);
        }
        if ( key.equals(ParameterNameEnum.PASSED.getName()) ) {
            message = setPassedResponse(session, message);
        }
        return message;
    }

    private static String setPassedResponse(Session session, String message) {
        User player = (User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName());
        User partner = (User)session.getUserProperties().get(ParameterNameEnum.USER.getName());
        //own points
        message += resourceBundle.getString(ParameterNameEnum.DISPLAY_OWN_POINTS.getName()) +  " " + player.getCurrentPoints()
                + ParameterNameEnum.BR.getName();
        //own cards
        message = addCardsToMessage(message, player);
        message = addNumberOfCardsToMessage(message, partner);
        message = addCardBackToMessage(message, partner);
        return message;
    }

    private static String addCardBackToMessage(String message, User partner) {
        for (int i = 0; i < partner.getDrawnCards().size(); i++) {
            message += resourceBundle.getString(ParameterNameEnum.CARD_BACK.getName());
        }
        return message;
    }

    private static String addNumberOfCardsToMessage(String message, User partner) {
        if (partner.getDrawnCards().size() > 0) {
            message += resourceBundle.getString(ParameterNameEnum.PARTNER_DREW.getName()) + " "
                    + partner.getDrawnCards().size() + " "
                    + resourceBundle.getString(ParameterNameEnum.CLOSE_PARTNER_DREW.getName());
        }
        return message;
    }

    private static String addCardsToMessage(String message, User player) {
        for (CardEnum drawnCard : player.getDrawnCards()) {
            message += resourceBundle.getString(ParameterNameEnum.OPEN_CARD_IMAGE_TAG.getName())
                    + drawnCard.getImageUrl()
                    + resourceBundle.getString(ParameterNameEnum.CLOSE_CARD_IMAGE_TAG.getName());
        }
        return message;
    }

    private static String setGameOverResponse(Session session, String message) {
        User player = (User)session.getUserProperties().get(ParameterNameEnum.USER.getName());
        User partner;
        try {
            partner = (User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName());
        } catch (Exception e) {
            partner = PvpGameManager.getPlayer((String)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName()));
        }
        if (player.getGamingStatus() == GamingStatusEnum.PLAYER_LOST){
            message = setPlayerLostResponse(message, player);
        }  /*if player lost */ else {
            message = setPlayerWonResponse(message, player, partner);
        }
        message = addGameOverInfoToMessage(message, player, partner);
        return message;
    }

    private static String addGameOverInfoToMessage(String message, User player, User partner) {
        //display points
        message += resourceBundle.getString(ParameterNameEnum.DISPLAY_POINTS.getName()) + " "
                + player.getCurrentPoints();
        //your rating is
        message += resourceBundle.getString(ParameterNameEnum.RATING.getName()) + " " + player.getRating();
        //partner's points
        message += resourceBundle.getString(ParameterNameEnum.PARTNER_POINTS.getName()) + " "
                + partner.getCurrentPoints();
        //partner's cards
        if (partner.getDrawnCards().size() > 0) {
            message += resourceBundle.getString(ParameterNameEnum.DISPLAY_PARTNER_POINTS.getName());
        }
        message = addCardsToMessage(message, partner);
        //main page button
        message += resourceBundle.getString(ParameterNameEnum.MAIN_PAGE_BUTTON.getName());
        //still display cards
        message = addCardsToMessage(message, player);
        return message;
    }

    private static String setPlayerWonResponse(String message, User player, User partner) {
        message += resourceBundle.getString(ParameterNameEnum.PLAYER_WON.getName());
        //you've gained n money
        BigDecimal money = player.getCurrentBet().add(partner.getCurrentBet());
        message += resourceBundle.getString(ParameterNameEnum.GAINED_MONEY.getName()) + " "
                +  money.toString();
        return message;
    }

    private static String setPlayerLostResponse(String message, User player) {
        //general message
        message += resourceBundle.getString(ParameterNameEnum.PLAYER_LOST.getName());
        //you've lost n money
        message += resourceBundle.getString(ParameterNameEnum.LOST_MONEY.getName()) + " "
                +  player.getCurrentBet().toString();
        return message;
    }

    private static String setDisplayPointsResponse(Session session, String message) {
        User player;
        User partner;
        try {
            player = (User)session.getUserProperties().get(ParameterNameEnum.USER.getName());
            partner = (User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName());
        } catch (Exception e ) {
            player = (User)session.getUserProperties().get(ParameterNameEnum.USER.getName());
            partner = PvpGameManager.getPartner(player.getCurrentGame(),
                                            ((User)session.getUserProperties().get(ParameterNameEnum.USER.getName())).getLogin());
        }
        message += " " + player.getCurrentPoints();
        message += resourceBundle.getString(ParameterNameEnum.DISPLAY_CARDS_BUTTON.getName());
        if (partner.getGamingStatus() != GamingStatusEnum.PASSED) {
            message += resourceBundle.getString(ParameterNameEnum.DISPLAY_PASS_BUTTON.getName());
        }
        message = addCardsToMessage(message, player);
        if (partner.getDrawnCards().size() > 0) {
            message += resourceBundle.getString(ParameterNameEnum.DISPLAY_PARTNER_POINTS.getName());
        }
        message = addCardBackToMessage(message, partner);
        return message;
    }

    private static String setRegisteredSecondResponse(Session session, String message) {
        message += ((User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName())).getLogin() + " ";
        return message;
    }
}
