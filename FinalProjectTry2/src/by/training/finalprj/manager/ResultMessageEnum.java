package by.training.finalprj.manager;

/**
 * Enum of all possible result message names
 */
public enum ResultMessageEnum {
    WRONG_ACTION_MESSAGE("errormessage.wrongaction"),
    LOGIC_ERROR_MESSAGE("errormessage.logicerror"),
    NULL_PAGE_MESSAGE("errormessage.nullpage"),
    //websocket stuff
    PLAYER_LEFT("playerLeftEarly"),
    LOGIC_EXCEPTION("logicException"),
    WAITING_OTHER_BET("waitingOtherBet"),
    GAME_STARTED("gameStarted"),
    UNABLE_TO_BET("unableToBet"),
    REGISTERED_FIRST("registeredFirst"),
    REGISTERED_SECOND("registeredSecond"),
    UNABLE_TO_REGISTER("Unable to register user"),
    PASSED("passed"),
    DISPLAY_POINTS("displayPoints"),
    GAME_OVER("gameOver"),
    CARD_DRAWING_ERROR("cardDrawingError"),
    ERROR("error"),
    //ai game websocket stuff
    AI_WELCOME_MESSAGE("AIWelcomeMessage"),
    AI_DISPLAY_POINTS("AIDisplayPoints"),
    AI_GAME_OVER("AIGameOver"),
    AI_CARD_DRAWING_ERROR("AICardDrawingError"),
    AI_GAME_OVER_USER_WON("AIGameOverUserWon"),
    AI_GAME_OVER_USER_LOST("AIGameOverUserLost"),
    AI_COMPUTER_POINTS("AIComputerPoints"),
    AI_COMPUTER_CARDS("AIComputerCards"),
    //non-websocket stuff
    OPERATION_SUCCESSFUL("Operation successful."),
    OPERATION_FAILED("Operation failed");

    private String name;

    ResultMessageEnum(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
