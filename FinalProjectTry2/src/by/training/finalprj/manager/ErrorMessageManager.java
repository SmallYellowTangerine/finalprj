package by.training.finalprj.manager;

import java.util.ResourceBundle;

/**
 * Util class for fetching error messages from properties file
 */
public class ErrorMessageManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(ParameterNameEnum.ERRORMESSAGES.getName());

    private ErrorMessageManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}