package by.training.finalprj.manager;

import by.training.finalprj.entity.CardEnum;
import by.training.finalprj.entity.GamingStatusEnum;
import by.training.finalprj.entity.User;

import javax.websocket.Session;
import java.math.BigDecimal;
import java.util.ResourceBundle;

/**
 * Util class for managing response to current user's partner in pvp game logic
 */
public class WsPartnerResponseManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(ParameterNameEnum.WSPARTNER.getName());

    private WsPartnerResponseManager() { }

    public static String getProperty(String key, Session session) {
        String message = "";
        //partner
        User partner = ((User)session.getUserProperties().get(ParameterNameEnum.PARTNER.getName()));
        if ( key.equals(ParameterNameEnum.REGISTERED_SECOND.getName()) ) {
            message = setRegisteredSecondResponse(session, message);
        }
        message += resourceBundle.getString(key);
        //your partner drew n cards
        if ( key.equals(ParameterNameEnum.DISPLAY_POINTS.getName()) ){
            message = setDisplayPointsResponse(session, message, partner);
        }
        if ( key.equals(ParameterNameEnum.GAME_OVER.getName()) ) {
            message = setGameOverResponse(session, message, partner);
        }
        //this is a partner still
        if ( key.equals(ParameterNameEnum.PASSED.getName()) ) {
            message = setPassedResponse(message, partner);
        }
        return message;
    }

    private static String setPassedResponse(String message, User partner) {
        message += resourceBundle.getString(ParameterNameEnum.DISPLAY_CARDS_BUTTON.getName());
        //former player's cards
        for (int i = 0; i < partner.getDrawnCards().size(); i++) {
            message += resourceBundle.getString(ParameterNameEnum.CARD_BACK.getName());
        }
        return message;
    }

    private static String setGameOverResponse(Session session, String message, User partner) {
        //player
        User player = ((User)session.getUserProperties().get(ParameterNameEnum.USER.getName()));
        //player lost, partner won
        if (player.getGamingStatus() == GamingStatusEnum.PLAYER_LOST) {
            message = setPlayerLostResponse(message, partner, player);
        } else { //partner (this user) won
            message = setPlayerWonResponse(message, partner, player);
        }
        message = addGameOverInfoToMessage(message, partner, player);
        return message;
    }

    private static String addGameOverInfoToMessage(String message, User partner, User player) {
        //your points
        message += resourceBundle.getString(ParameterNameEnum.DISPLAY_OWN_POINTS.getName()) +  " " + partner.getCurrentPoints();
        //display player's points
        message += resourceBundle.getString(ParameterNameEnum.PLAYER_POINTS.getName()) + player.getCurrentPoints();
        //your rating is
        message += resourceBundle.getString(ParameterNameEnum.RATING.getName()) + " " + partner.getRating() + ParameterNameEnum.BR.getName();
        //own cards
        message = addCardsToMessage(message, partner);
        //main page button
        message += resourceBundle.getString(ParameterNameEnum.MAIN_PAGE_BUTTON.getName());
        //player's cards
        message  += resourceBundle.getString(ParameterNameEnum.PLAYER_CARDS.getName());
        message = addCardsToMessage(message, player);
        return message;
    }

    private static String addCardsToMessage(String message, User partner) {
        for (CardEnum drawnCard : partner.getDrawnCards()) {
            message += resourceBundle.getString(ParameterNameEnum.OPEN_CARD_IMAGE_TAG.getName())
                    + drawnCard.getImageUrl()
                    + resourceBundle.getString(ParameterNameEnum.CLOSE_CARD_IMAGE_TAG.getName());
        }
        return message;
    }

    private static String setPlayerWonResponse(String message, User partner, User player) {
        //you've lost and lost money
        message += resourceBundle.getString(ParameterNameEnum.PLAYER_WON.getName());
        BigDecimal money = player.getCurrentBet().add(partner.getCurrentBet());
        message += money.toString();
        return message;
    }

    private static String setPlayerLostResponse(String message, User partner, User player) {
        message += resourceBundle.getString(ParameterNameEnum.PLAYER_LOST.getName());
        //you've won and gained money
        BigDecimal money = player.getCurrentBet().add(partner.getCurrentBet());
        message += money.toString();
        return message;
    }

    private static String setDisplayPointsResponse(Session session, String message, User partner) {
        //player
        User player = ((User)session.getUserProperties().get(ParameterNameEnum.USER.getName()));
        message += " " + player.getDrawnCards().size() + " ";
        message += resourceBundle.getString(ParameterNameEnum.CLOSE_DISPLAY_POINTS.getName());
        for (int i = 0; i < player.getDrawnCards().size(); i++) {
            message += resourceBundle.getString(ParameterNameEnum.CARD_BACK.getName());
        }
        message += ParameterNameEnum.BR.getName() + ParameterNameEnum.BR.getName();
        //your points
        message += resourceBundle.getString(ParameterNameEnum.DISPLAY_OWN_POINTS.getName()) +  " " + partner.getCurrentPoints() + "<br/>";
        //own cards
        message = addCardsToMessage(message, partner);
        return message;
    }

    private static String setRegisteredSecondResponse(Session session, String message) {
        message += ((User)session.getUserProperties().get(ParameterNameEnum.USER.getName())).getLogin() + " ";
        return message;
    }
}
