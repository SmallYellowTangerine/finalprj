package by.training.finalprj.manager;

import by.training.finalprj.entity.CardEnum;
import by.training.finalprj.entity.User;
import by.training.finalprj.logic.AiGameLogic;

import javax.websocket.Session;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static by.training.finalprj.entity.GamingStatusEnum.AI_GAME_USER_WON;
import static by.training.finalprj.logic.AiGameLogic.activeGamesCardLists;

/**
 * Util class for managing response to user in AI vs user game logic
 */
public class AiGameResponseManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle(ParameterNameEnum.AI_GAME.getName());

    private AiGameResponseManager() { }

    public static String getProperty(String key, Session session) {
        AiGameLogic aiGameLogic = new AiGameLogic();
        User user;
        if (key.equals(ResultMessageEnum.LOGIC_ERROR_MESSAGE.getName())) {
            return resourceBundle.getString(key);
        }
        String message = "";
        message += resourceBundle.getString(key);
        if ( key.equals(ResultMessageEnum.AI_DISPLAY_POINTS.getName()) ) {
            user = (User)session.getUserProperties().get(ParameterNameEnum.USER.getName());
            message = setDisplayPointsResponse(user, message);
        }
        if (key.equals(ResultMessageEnum.AI_GAME_OVER.getName())) {
            user = (User)session.getUserProperties().get(ParameterNameEnum.USER.getName());
            message = setDisplayPointsResponse(user, message);
            message = setGameOverResponse(user, message);
            aiGameLogic.endGame(user);
        }
        return message;
    }

    private static String setGameOverResponse(User user, String message) {
        message += user.getGamingStatus().equals(AI_GAME_USER_WON)
                ? resourceBundle.getString(ResultMessageEnum.AI_GAME_OVER_USER_WON.getName())
                : resourceBundle.getString(ResultMessageEnum.AI_GAME_OVER_USER_LOST.getName());
        message += resourceBundle.getString(ResultMessageEnum.AI_COMPUTER_POINTS.getName()) +
                getComputerPoints(activeGamesCardLists.get(user.getLogin()));
        if (activeGamesCardLists.get(user.getLogin()) != null) {
            message += resourceBundle.getString(ResultMessageEnum.AI_COMPUTER_CARDS.getName());
            message = addCardsToMessage(message, activeGamesCardLists.get(user.getLogin()));
        }
        return message;
    }

    private static String setDisplayPointsResponse(User user, String message) {
        message += " " + user.getCurrentPoints();
        message = addCardsToMessage(message, user.getDrawnCards());
        return message;
    }

    private static String addCardsToMessage(String message, ArrayList<CardEnum> cards) {
        message += "</br>";
        for (CardEnum drawnCard : cards) {
            message += resourceBundle.getString(ParameterNameEnum.OPEN_CARD_IMAGE_TAG.getName())
                    + drawnCard.getImageUrl()
                    + resourceBundle.getString(ParameterNameEnum.CLOSE_CARD_IMAGE_TAG.getName());
        }
        return message;
    }

    private static int getComputerPoints(ArrayList<CardEnum> cards) {
        int points = 0;
        if (cards == null) {
            return points;
        }
        for (CardEnum drawnCard : cards) {
            points += drawnCard.getPoints();
        }
        return  points;
    }
}
