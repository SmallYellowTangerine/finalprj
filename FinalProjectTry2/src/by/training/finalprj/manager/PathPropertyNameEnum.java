package by.training.finalprj.manager;

/**
 * Enum of all possible path names
 */
public enum PathPropertyNameEnum {
    VIEW_DIALOGUES("path.page.viewdialogues"),
    VIEW_MESSAGES("path.page.viewmessages"),
    VIEW_USERS("path.page.viewusers"),
    LOGIN("path.page.login"),
    MAIN("path.page.main"),
    INDEX("path.page.index"),
    REPLENISH_BALANCE("path.page.moneytransfer"),
    STATISTICS("path.page.statistics");

    private String name;

    PathPropertyNameEnum(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
