package by.training.finalprj.dao;

import by.training.finalprj.database.ConnectionPool;
import by.training.finalprj.entity.GameUser;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;

/**
 * This is a DAO class which provides access to transaction table of the database
 */
public class TransactionDAO extends AbstractDAO {

    private static Logger LOGGER = LogManager.getRootLogger();

    private static final String INSERT_TRANSACTION = "INSERT INTO finalschema.transaction VALUES (NULL, ?, ?, ?, ?)";

    public Boolean insertTransaction(User user, BigDecimal amount) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(INSERT_TRANSACTION);
            //get player_game
            UserGameDAO userGameDAO = new UserGameDAO();
            GameUser gameUser = userGameDAO.insertOrFindPlayerGame(user, user.getCurrentGame());
            if (gameUser == null) {
                throw new DAOException("GameUser object not created");
            }
            preparedStatement.setString(1, amount.abs().toString());
            preparedStatement.setString(2, LocalDateTime.now().toString());
            preparedStatement.setBoolean(3, amount.compareTo(BigDecimal.valueOf(0)) < 0);
            preparedStatement.setInt(4, gameUser.getId());
            Integer i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Inserted transaction to db");
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't insert transaction to db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}
