package by.training.finalprj.dao;

import by.training.finalprj.database.ConnectionPool;
import by.training.finalprj.entity.Game;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;

/**
 * This is a DAO class which provides access to game table of the database
 */
public class GameDAO extends AbstractDAO {

    private static final Logger LOGGER = LogManager.getRootLogger();

    private static final String INSERT_GAME = "INSERT INTO finalschema.game VALUES (NULL, ?, ?, ?)";
    private static final String UPDATE_GAME = "UPDATE finalschema.game SET g_status=? WHERE g_id=?";
    private static final String SELECT_LAST_INSERT = "SELECT MAX(g_id) FROM finalschema.game";
    private static final String FINAL_UPDATE_GAME = "UPDATE finalschema.game SET g_winner=?, g_status=? WHERE g_id=?";
    private static final String MAX_ID_COLUMN_LABEL = "MAX(g_id)";

    private Game extractGameFromResultSet(ResultSet resultSet) throws SQLException, DAOException {
        Game game = new Game();
        game.setId( resultSet.getInt(MAX_ID_COLUMN_LABEL) );
        return game;
    }

    public boolean insertGame(Game game, Integer winnerId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(INSERT_GAME);
            preparedStatement.setString(1, LocalDateTime.now().toString());
            if (winnerId == null) {
                preparedStatement.setString(2, null);
            } else {
                preparedStatement.setInt(2, winnerId);
            }
            preparedStatement.setString(3, game.getGameStatus().toString());
            Integer i = preparedStatement.executeUpdate();
            //set game object id
            preparedStatement = connection.prepareStatement(SELECT_LAST_INSERT);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                game.setId(extractGameFromResultSet(resultSet).getId());
                LOGGER.info("Game with id " + game.getId() + " inserted to db");
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't insert game to db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public boolean updateGame(Game game) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_GAME);
            preparedStatement.setString(1, game.getGameStatus().toString());
            preparedStatement.setInt(2, game.getId());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Game with id " + game.getId() + " updated in db");
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't update game in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public boolean finalUpdateGame(Game game) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(FINAL_UPDATE_GAME);
            if (game.getWinner() ==  null) {
                preparedStatement.setNull(1, Types.INTEGER);
            } else {
                preparedStatement.setInt(1, game.getWinner().getId());
            }
            preparedStatement.setString(2, game.getGameStatus().toString());
            preparedStatement.setInt(3, game.getId());
            int i = preparedStatement.executeUpdate();
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't search for game in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}
