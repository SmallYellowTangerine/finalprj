package by.training.finalprj.dao;

import by.training.finalprj.entity.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This is an abstract class for DAO classes which implemets some common methods
 */
public abstract class AbstractDAO <K, T extends Entity> {

    private final static Logger LOGGER = LogManager.getRootLogger();

    public void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.warn("Couldn't close statement");
        }
    }

    public void close(Connection connection) throws DAOException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
