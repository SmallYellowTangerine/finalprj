package by.training.finalprj.dao;

import by.training.finalprj.database.ConnectionPool;
import by.training.finalprj.entity.Message;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

import static by.training.finalprj.dao.UserDAO.*;

/**
 * This is a DAO class which provides access to message table of the database
 */
public class MessageDAO extends AbstractDAO {

    private static final Logger LOGGER = LogManager.getRootLogger();

    private static final String SELECT_ALL_MESSAGES = "SELECT * FROM finalschema.message WHERE m_sender = ? OR m_receiver = ?";
    private static final String SELECT_ONE_DIALOGUE = "SELECT * FROM finalschema.message WHERE m_sender IN (?, ?) AND m_receiver IN (?, ?);";
    private static final String INSERT_MESSAGE = "INSERT INTO finalschema.message VALUES (NULL, ?, ?, ?, ?, ?)";
    private static final String SELECT_MESSAGE_BY_ID = "SELECT * FROM finalschema.message WHERE m_id=?";
    private static final String UPDATE_MESSAGE_READ = "UPDATE finalschema.message SET m_read=? WHERE m_id=?";
    private static final String ID_COLUMN_LABEL = "m_id";
    private static final String TEXT_COLUMN_LABEL = "m_text";
    private static final String DATE_TIME_COLUMN_LABEL = "m_date_time";
    private static final String SENDER_COLUMN_LABEL = "m_sender";
    private static final String RECEIVER_COLUMN_LABEL = "m_receiver";
    private static final String READ_COLUMN_LABEL = "m_read";

    private Message extractMessageFromResultSet(ResultSet resultSet) throws SQLException, DAOException {
        Message message = new Message();
        UserDAO userDao = new UserDAO();
        message.setId(resultSet.getInt(ID_COLUMN_LABEL));
        message.setText(resultSet.getString(TEXT_COLUMN_LABEL));
        message.setSentDateTime(resultSet.getTimestamp(DATE_TIME_COLUMN_LABEL).toLocalDateTime());
        message.setSender(userDao.getUser(resultSet.getInt(SENDER_COLUMN_LABEL)).getLogin());
        message.setReceiver(userDao.getUser(resultSet.getInt(RECEIVER_COLUMN_LABEL)).getLogin());
        message.setRead(resultSet.getBoolean(READ_COLUMN_LABEL));
        return message;
    }

    public Message getMessage(int id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_MESSAGE_BY_ID);
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return extractMessageFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't get message by id from db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return null;
    }

    public ArrayList<Message> findMessagesByUser(int id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Message> messages = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_ALL_MESSAGES);
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.setString(2, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                messages.add(extractMessageFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find message by user in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return messages;
    }

    public ArrayList<Message> findMessagesByTwoUsers(int id, int partnerId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Message> messages = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_ONE_DIALOGUE);
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.setString(2, String.valueOf(partnerId));
            preparedStatement.setString(3, String.valueOf(partnerId));
            preparedStatement.setString(4, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                messages.add(extractMessageFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find messages by two users in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return messages;
    }

    public Boolean insertMessage(Message newMessage) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        UserDAO userDao = new UserDAO();
        try {
            User user = userDao.findUserByName(newMessage.getSender());
            User partner = userDao.findUserByName(newMessage.getReceiver());
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(INSERT_MESSAGE);
            preparedStatement.setString(1, newMessage.getText());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(newMessage.getSentDateTime()));
            preparedStatement.setInt(3, user.getId());
            preparedStatement.setInt(4, partner.getId());
            preparedStatement.setBoolean(5, newMessage.isRead());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Inserted message to db");
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't insert message to db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public boolean updateMessageRead(Message message) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_MESSAGE_READ);
            preparedStatement.setBoolean(1, message.isRead());
            preparedStatement.setInt(2, message.getId());
            int i = preparedStatement.executeUpdate();
            if (i != 1) {
                message.setRead(!message.isRead());
            } else {
                LOGGER.info("Updated message's read property, message id " + message.getId());
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't update message's read property", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}
