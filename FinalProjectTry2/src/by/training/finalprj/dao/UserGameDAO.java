package by.training.finalprj.dao;

import by.training.finalprj.database.ConnectionPool;
import by.training.finalprj.entity.Game;
import by.training.finalprj.entity.GameUser;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is a DAO class which provides access to usergame table of the databaseCreated by Администратор on 14.05.2017.
 */
public class UserGameDAO extends AbstractDAO {

    private static final Logger LOGGER = LogManager.getRootLogger();

    private static final String INSERT_PLAYER_GAME = "INSERT INTO finalschema.game_user VALUES (NULL, ?, ?)";
    private static final String SELECT_LAST_INSERT = "SELECT MAX(gu_id) AS gu_id FROM finalschema.game_user";
    private static final String SELECT_PLAYER_GAME_WITH_GAME_ID = "SELECT * FROM finalschema.game_user WHERE g_id=?";
    private static final String ID_COLUMN_LABEL = "gu_id";

    private GameUser extractPlayerGameFromResultSet(ResultSet resultSet) throws SQLException, DAOException {
        GameUser gameUser = new GameUser();
        gameUser.setId( resultSet.getInt(ID_COLUMN_LABEL) );
        return gameUser;
    }

    public GameUser insertOrFindPlayerGame(User user, Game game) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        GameUser gameUser;
        try {
            gameUser = findGameUserByGameId(game.getId());
            if (gameUser == null) {
                connection = ConnectionPool.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(INSERT_PLAYER_GAME);
                preparedStatement.setInt(1, user.getId());
                preparedStatement.setInt(2, game.getId());
                preparedStatement.executeUpdate();
                //set game object id
                preparedStatement = connection.prepareStatement(SELECT_LAST_INSERT);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    gameUser = extractPlayerGameFromResultSet(resultSet);
                }
                LOGGER.info("Inserted playergame to db, playergame id " + gameUser.getId());
            }
            if (gameUser != null) {
                gameUser.setGameId( game.getId() );
                gameUser.setPlayerId( user.getId() );
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't insert playergame to db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return gameUser;
    }

    private GameUser findGameUserByGameId(int gameId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        GameUser gameUser = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_PLAYER_GAME_WITH_GAME_ID);
            preparedStatement.setInt(1, gameId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                gameUser = extractPlayerGameFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find playergame by game id" + gameId, e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return gameUser;
    }
}
