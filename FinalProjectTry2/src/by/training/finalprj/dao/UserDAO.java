package by.training.finalprj.dao;

import by.training.finalprj.database.ConnectionPool;
import by.training.finalprj.entity.GamingStatusEnum;
import by.training.finalprj.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

/**
 * This is a DAO class which provides access to user table of the database
 */
public class UserDAO extends AbstractDAO {

    private final Logger LOGGER = LogManager.getRootLogger();

    private static final String SELECT_USER_BY_ID = "SELECT * FROM finalschema.user WHERE u_id=?";
    private static final String SELECT_USER_BY_NAME_PASSWORD = "SELECT * FROM finalschema.user WHERE u_login=? AND u_password=?";
    private static final String INSERT_USER = "INSERT INTO finalschema.user VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_USER_BY_NAME = "SELECT * FROM finalschema.user WHERE u_login=?";
    private static final String SELECT_ALL_USERS = "SELECT * FROM finalschema.user";
    private static final String UPDATE_USER_ROLE = "UPDATE finalschema.user SET u_role=? WHERE u_login=?";
    private static final String SELECT_USERS_WITH_ROLE = "SELECT * FROM finalschema.user WHERE u_role=?";
    private static final String DELETE_USER = "DELETE FROM finalschema.user WHERE u_login=?";
    private static final String UPDATE_GAMES_WON = "UPDATE finalschema.user SET u_games_won=? WHERE u_id=?";
    private static final String UPDATE_GAMES_LOST = "UPDATE finalschema.user SET u_games_lost=? WHERE u_id=?";
    private static final String UPDATE_RATING = "UPDATE finalschema.user SET u_rating=? WHERE u_id=?";
    private static final String UPDATE_BALANCE = "UPDATE finalschema.user SET u_balance=? WHERE u_id=?";

    private static final String ID_COLUMN_LABEL = "u_id";
    private static final String LOGIN_CARD_NUMBER_COLUMN_LABEL = "u_login";
    private static final String PASSWORD_COLUMN_LABEL = "u_password";
    private static final String EMAIL_COLUMN_LABEL = "u_email";
    private static final String REGISTRATION_DATE_COLUMN_LABEL = "u_registration_date";
    private static final String ROLE_COLUMN_LABEL = "u_role";
    private static final String BANK_CARD_NUMBER_COLUMN_LABEL = "u_bank_card_number";
    private static final String BALANCE_COLUMN_LABEL = "u_balance";
    private static final String RATING_COLUMN_LABEL = "u_rating";
    private static final String GAMES_WON_COLUMN_LABEL = "u_games_won";
    private static final String GAMES_LOST_COLUMN_LABEL = "u_games_lost";

    private static final double RATING_REDUCTION = 3;
    private static final double RATING_ADDITION = 5;

    private User extractUserFromResultSet(ResultSet resultSet) throws SQLException, DAOException {
        User user = new User();
        user.setId(resultSet.getInt(ID_COLUMN_LABEL));
        user.setLogin(resultSet.getString(LOGIN_CARD_NUMBER_COLUMN_LABEL));
        user.setPassword(resultSet.getString(PASSWORD_COLUMN_LABEL));
        user.setEmail(resultSet.getString(EMAIL_COLUMN_LABEL));
        user.setRegistrationDate(resultSet.getDate(REGISTRATION_DATE_COLUMN_LABEL).toLocalDate());
        user.setRole(resultSet.getInt(ROLE_COLUMN_LABEL));
        user.setBankCardNumber( resultSet.getString(BANK_CARD_NUMBER_COLUMN_LABEL) );
        user.setBalance( resultSet.getBigDecimal(BALANCE_COLUMN_LABEL) );
        user.setRating( resultSet.getDouble(RATING_COLUMN_LABEL) );
        user.setGamesWon(resultSet.getInt(GAMES_WON_COLUMN_LABEL));
        user.setGamesLost(resultSet.getInt(GAMES_LOST_COLUMN_LABEL));
        return user;
    }

    User getUser(int id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return extractUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find user by id in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return null;
    }

    public User findUserByNameAndPassword(String login, String password) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        User user = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_USER_BY_NAME_PASSWORD);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = extractUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find user by name and password in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return user;
    }

    public Boolean insertUser(User user) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(INSERT_USER);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setDate(4, Date.valueOf(user.getRegistrationDate()));
            preparedStatement.setInt(5, user.getRole());
            preparedStatement.setString(6, user.getBankCardNumber());
            preparedStatement.setBigDecimal(7, user.getBalance());
            preparedStatement.setDouble(8, user.getRating());
            preparedStatement.setInt(9, user.getGamesWon());
            preparedStatement.setInt(10, user.getGamesLost());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                //to set id
                int userId = findUserByName(user.getLogin()).getId();
                LOGGER.info("Inserted user to db, user id " + userId);
                user.setId(userId);
            }
            return i == 1;
        } catch (SQLException e) {
            throw new DAOException("Couldn't insert user to db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public User findUserByName(String login) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        User user = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_USER_BY_NAME);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = extractUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find user by name from db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return user;
    }

    public ArrayList<User> getAllUsers() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<User> users = new ArrayList<User>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                users.add(extractUserFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't get all users from db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return users;
    }

    public Boolean updateRole(String username, int newRole) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        User userToBlock = findUserByName(username);
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_USER_ROLE);
            preparedStatement.setString(1, String.valueOf(newRole));
            preparedStatement.setString(2, username);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Updated user's role property in db for user " + username);
                userToBlock.setRole(newRole);
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't update role in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public ArrayList<User> getAllUsersWithRole(int role) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<User> usersWithRole = new ArrayList<User>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(SELECT_USERS_WITH_ROLE);
            preparedStatement.setInt(1, role);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                usersWithRole.add(extractUserFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find all users with certain role in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return usersWithRole;
    }

    public boolean deleteUser(String username) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(DELETE_USER);
            preparedStatement.setString(1, username);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Deleted user " + username + " from database successfully");
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't delete user", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public Boolean updateGamesWonLost(User player) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Integer gamesChanged;
        String statement;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            if (player.getGamingStatus() == GamingStatusEnum.PLAYER_LOST) {
                gamesChanged = player.getGamesLost() + 1;
                statement = UPDATE_GAMES_LOST;
            } else {
                gamesChanged = player.getGamesWon() + 1;
                statement = UPDATE_GAMES_WON;
            }
            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setString(1, gamesChanged.toString());
            preparedStatement.setString(2, String.valueOf(player.getId()));
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Updated games won/lost in db for user with id " + player.getId());
                if (player.getGamingStatus() == GamingStatusEnum.PLAYER_LOST) {
                    player.setGamesLost(gamesChanged);
                } else {
                    player.setGamesWon(gamesChanged);
                }
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't update games won/lost in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    public Boolean updateRating(User player) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Double changedRating;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_RATING);
            changedRating = decideRatingChange(player);
            preparedStatement.setString(1, changedRating.toString());
            preparedStatement.setString(2, String.valueOf(player.getId()));
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Updated rating in db for user with id " + player.getId());
                player.setRating(changedRating);
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't update rating in db", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    private Double decideRatingChange(User player) {
        Double changedRating;
        if (player.getGamingStatus() == GamingStatusEnum.PLAYER_LOST) {
            changedRating = player.getRating() - RATING_REDUCTION;
            if (changedRating < 0) {
                changedRating = 0d;
            }
        } else {
            changedRating = player.getRating() + RATING_ADDITION;
        }
        return changedRating;
    }

    public Boolean updateBalance(User player, BigDecimal sum) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_BALANCE);
            preparedStatement.setString(1, sum.toString());
            preparedStatement.setString(2, String.valueOf(player.getId()));
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                LOGGER.info("Updated balance to db for playerprofile with id " + player.getId());
                player.setBalance(sum);
            }
            return (i == 1);
        } catch (SQLException e) {
            throw new DAOException("Couldn't update balance", e);
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}
