<%--
  Created by IntelliJ IDEA.
  User: Администратор
  Date: 02.04.2017
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lcl}" />
<fmt:setBundle basename="resources.MessagesBundle"/>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>21</title>

    <link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <link href="/css/styletry2.css" rel="stylesheet">
</head>

<body>

<div class="container whitebg">

    <div id="playersearch"></div>

</div>

<div class="container whitebg">
    <footer>
        <hr>
        <div class="container">© Egorova Anastasia 2017</div>
    </footer>
</div>

<script type="text/javascript" src="/js/pvpwebsockettry2.js"></script>

</body>
</html>
