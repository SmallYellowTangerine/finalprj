<%--
  Created by IntelliJ IDEA.
  User: Администратор
  Date: 17.04.2017
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lcl}" />
<fmt:setBundle basename="resources.MessagesBundle"/>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>21</title>

    <link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <link href="/css/styletry2.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#"><fmt:message key="menu"/></a>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/./jsp/main.jsp"><fmt:message key="home"/></a>
            </li>
            <c:if test="${userType != 'BLOCKED_USER' && loggedIn == true}">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./Controller?command=STATISTICS"><fmt:message key="playersRating"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./jsp/user/game.jsp"><fmt:message key="startPlaying"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./jsp/user/aigame.jsp"><fmt:message key="aiStartPlaying"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/jsp/user/balance.jsp"><fmt:message key="replenishBalance"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./Controller?command=DISPLAY_DIALOGUES"><fmt:message key="messages"/></a>
                </li>
            </c:if>
            <c:if test="${userType == 'ADMIN'}">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./Controller?command=DISPLAY_USERS"><fmt:message key="adminViewAllUsers"/></a>
                </li>
            </c:if>
        </ul>
        <c:choose>
            <c:when test="${loggedIn == true}">
                <form action="${pageContext.request.contextPath}/./Controller" method="POST">
                    <input type="hidden" name="command" value="logout" />
                    <input type="submit" value=<fmt:message key="logout" /> />
                </form>
            </c:when>
            <c:otherwise>
                <a class="btn smallbutton" href="${pageContext.request.contextPath}/./jsp/login.jsp" role="button"><fmt:message key="signIn"/></a>
                <a class="btn smallbutton" href="${pageContext.request.contextPath}/./jsp/registration.jsp" role="button"><fmt:message key="signUp"/></a>
            </c:otherwise>
        </c:choose>
        <form name="LocalizationForm" action="${pageContext.request.contextPath}/./Controller" method="POST">
            <input type="hidden" name="command" value="localize" />
            <div class="inputs">
                <input class="btn smallbutton" type="submit" name="ru" value="Ru"/>
                <input class="btn smallbutton" type="submit" name="en" value="En"/>
            </div>
        </form>
    </div>
</nav>

<div class="container whitebg">
    <form name="DialogueSearchForm" action="${pageContext.request.contextPath}/Controller" method="POST">
        <input type="hidden" name="command" value="SEARCH_DIALOGUE" />
        <div class="inputs">
            <p><fmt:message key="searchForUsertoMessage"/> </p>
            <input id="login" type="text" name="searchLogin" placeholder="<fmt:message key="usersLogin"/> " autofocus />
            <input type="submit" id="submit" value="<fmt:message key="search"/> " />
            <br/>
            ${wrongAction}
            ${nullPage}
            <br/>
        </div>
    </form>

    <form name="MessageAdminForm" action="${pageContext.request.contextPath}/Controller" method="POST">
        <input type="hidden" name="command" value="DISPLAY_ADMINS" />
        <div class="inputs">
            <input type="submit" id="submit" value="<fmt:message key="messageAdmin"/> " />
            <br/>
            ${wrongAction}
            ${nullPage}
            <br/>
        </div>
    </form>

    <div class="row">
        <div class="span8 offset2">
            <table id="list" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th><fmt:message key="partner"/> </th>
                    <th><fmt:message key="lastMessageSent"/> </th>
                    <th><fmt:message key="unreadMessages"/> </th>
                </tr>
                </thead><tbody>
                <c:forEach items="${dialogues}" var="dialogue">
                    <tr onclick="document.getElementById('${dialogue.partner}').submit();">
                        <td name="partnerLogin">
                            <form id="${dialogue.partner}" action="${pageContext.request.contextPath}/Controller" method="POST">
                                <input type="hidden" name="command" value="DISPLAY_MESSAGES">
                                <input type="hidden" name="partnerUsername" value="${dialogue.partner}">
                            </form>
                            <c:out value="${dialogue.partner}"/>
                        </td>
                        <td><c:out value="${dialogue.lastMessageDateTime}"/></td>
                        <td><c:out value="${dialogue.unreadMessages}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <br />
            <div id="pageNavPosition"></div>
            <br />
        </div>
    </div>
</div>

<div class="container whitebg">
    <footer>
        <hr>
        <div class="container">© Egorova Anastasia 2017</div>
    </footer>
</div>

<script type="text/javascript" src="/js/paginationtry1.js"></script>
<script type="text/javascript" src="/js/paginationtriggertry2.js"></script>
<script type="text/javascript" src="/js/f5protection.js"></script>
</body>
</html>


