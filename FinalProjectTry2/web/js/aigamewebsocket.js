/**
 * Created by Администратор on 01.09.2017.
 */
var webSocket = new WebSocket('ws://localhost:8088/aigamewebsocket');

webSocket.onerror = function(event) {
    onError(event)
};

webSocket.onopen = function(event) {
    onOpen(event)
};

webSocket.onmessage = function(event) {
    onMessage(event)
};



function onMessage(event) {
    document.getElementById('message').innerHTML
        = '<br />' + event.data;
}

function onOpen(event) {
    document.getElementById('message').innerHTML
        = 'Connection established';

}


function onError(event) {
    alert(event.data);
}

function drawCard() {
    var message = "drawCard";
    webSocket.send(message);
    return false;
}

function pass() {
    var message = "pass";
    webSocket.send(message);
    return false;
}
