/**
 * Created by Администратор on 18.08.2017.
 */
var message = null;

function validateForm() {
    var message = validateLogin() +
        validatePassword() +
        validateEmail() +
        validateCard();
    if (message) {
        alert(message);
        return false;
    } else {
        return true;
    }
}

function validateLogin() {
    var login = document.getElementById("login").value;
    var loginRegex = /^[a-zA-Z][a-zA-Z0-9_]{4,}$/;
    if ( !login.match(loginRegex) )
    {
        return "Incorrect login \n";
    } else {
        return "";
    }
}

function validatePassword() {
    var password = document.getElementById("password").value;;
    var passwordRegex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).+$/;
    if (password.length < 6 || !password.match(passwordRegex) )
    {
        return "Incorrect password \n";
    } else {
        return "";
    }
}

function validateEmail() {
    var email = document.getElementById("email").value;;
    if ( !(email.indexOf('.') > -1) || !(email.indexOf('@') > -1) )
    {
        return "Incorrect email \n";
    } else {
        return "";
    }
}

function validateCard() {
    var card = document.getElementById("card").value;
    var cardRegex = /^[0-9]{16}$/;
    if ( !card.match(cardRegex) )
    {
        return "Incorrect card number \n";
    } else {
        return "";
    }
}

function validateAccount() {
    var account = document.getElementById("account").value;
    var accountRegex = /^[0-9]+([,.][0-9]+)?$/g;
    if ( !account.match(accountRegex) )
    {
        alert("Invalid sum");
        return false;
    }
}