var webSocket = new WebSocket('ws://localhost:8088/messagewebsocket');

webSocket.onerror = function(event) {
    onError(event)
};

webSocket.onopen = function(event) {
    onOpen(event)
};

webSocket.onmessage = function(event) {
    onMessage(event)
};



function onMessage(event) {
    document.getElementById('newmessage').innerHTML
        = event.data + document.getElementById('newmessage').innerHTML;
}

function onOpen(event) {
}

function onError(event) {
    alert(event.data);
}


function sendMessage() {
    var message = document.getElementById('inputMessage').value;
    if (message.length < 1000) {
        webSocket.send(message);
        document.getElementById('inputMessage').value = "";
    } else {
        alert("Message can't be over 1000 symbols long!")
    }
    return false;
}
