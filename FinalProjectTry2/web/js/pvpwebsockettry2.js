/**
 * Created by Администратор on 05.09.2017.
 */

var webSocket = new WebSocket('ws://localhost:8088/websocket');

webSocket.onerror = function(event) {
    onError(event)
};

webSocket.onopen = function(event) {
    onOpen(event)
};

webSocket.onmessage = function(event) {
    onMessage(event)
};



function onMessage(event) {
    document.getElementById('playersearch').innerHTML
        = '<br />' + event.data;
}

function onOpen(event) {
    document.getElementById('playersearch').innerHTML
        = 'Connection established';

}


function onError(event) {
    alert(event.data);
}


function makeBet() {
    var message = document.getElementById('inputbet').value;
    var regexp = /^[0-9]+([,.][0-9]+)?$/g;
    if (message.match(regexp)) {
        webSocket.send(message);
    }
    return false;
}

function drawCard() {
    var message = "drawCard";
    webSocket.send(message);
    return false;
}

function pass() {
    var message = "pass";
    webSocket.send(message);
    return false;
}
