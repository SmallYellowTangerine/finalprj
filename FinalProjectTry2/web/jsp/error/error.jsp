<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lcl}" />
<fmt:setBundle basename="resources.MessagesBundle"/>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>21</title>

    <link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#"><fmt:message key="menu"/></a>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/./jsp/main.jsp"><fmt:message key="home"/></a>
            </li>
            <c:if test="${userType != 'BLOCKED_USER' && loggedIn == true}">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./Controller?command=STATISTICS"><fmt:message key="playersRating"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./jsp/user/game.jsp"><fmt:message key="startPlaying"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./jsp/user/aigame.jsp"><fmt:message key="aiStartPlaying"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/jsp/user/balance.jsp"><fmt:message key="replenishBalance"/></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./Controller?command=DISPLAY_DIALOGUES"><fmt:message key="messages"/></a>
                </li>
            </c:if>
            <c:if test="${userType == 'ADMIN'}">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/./Controller?command=DISPLAY_USERS"><fmt:message key="adminViewAllUsers"/></a>
                </li>
            </c:if>
        </ul>
        <c:choose>
            <c:when test="${loggedIn == true}">
                <form action="${pageContext.request.contextPath}/./Controller" method="POST">
                    <input type="hidden" name="command" value="logout" />
                    <input type="submit" value=<fmt:message key="logout" /> />
                </form>
            </c:when>
            <c:otherwise>
                <a class="btn smallbutton" href="${pageContext.request.contextPath}/./jsp/login.jsp" role="button"><fmt:message key="signIn"/></a>
                <a class="btn smallbutton" href="${pageContext.request.contextPath}/./jsp/registration.jsp" role="button"><fmt:message key="signUp"/></a>
            </c:otherwise>
        </c:choose>
        <form name="LocalizationForm" action="${pageContext.request.contextPath}/./Controller" method="POST">
            <input type="hidden" name="command" value="localize" />
            <div class="inputs">
                <input class="btn smallbutton" type="submit" name="ru" value="Ru"/>
                <input class="btn smallbutton" type="submit" name="en" value="En"/>
            </div>
        </form>
    </div>
</nav>

<div class="jumbotron">
    <div id="transparentbg" class="container text-center">
        <h1 class="display-3 bigtext"><fmt:message key="sorryError"/> </h1>
    </div>
</div>

<div class="container whitebg">
    <div class="row">
        <div class="col-md-10">
            <fmt:message key="requestFrom"/> ${pageContext.errorData.requestURI} <fmt:message key="isFailed"/>
            <br/>
            <fmt:message key="servletNameOrType"/> ${pageContext.errorData.servletName}
            <br/>
            <fmt:message key="statusCode"/> ${pageContext.errorData.statusCode}
            <br/>
            <fmt:message key="exceptionIs"/> ${pageContext.errorData.throwable}
        </div>
    </div>
</div>

<div class="container whitebg">
    <footer>
        <hr>
        <div class="container">© Egorova Anastasia 2017</div>
    </footer>
</div>


</body>
</html>

