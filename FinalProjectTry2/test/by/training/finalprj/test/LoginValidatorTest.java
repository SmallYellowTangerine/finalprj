package by.training.finalprj.test;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.logic.RegistrationLogic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Администратор on 06.09.2017.
 */
@RunWith(Parameterized.class)
public class LoginValidatorTest {

    @Parameterized.Parameters
    public static Collection<Object[]> connectionsAmount() {
        return Arrays.asList(new Object[][] {
                { "Vasya", true },
                { "Vasya123", true },
                { "abcdEfg", true },
                { "", false },
                { "321123", false },
                { "Vasya.com", false }
        });
    }

    private String input;
    private Boolean expected;

    public LoginValidatorTest(String input, Boolean expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void validateLogin() throws DAOException {
        RegistrationLogic registrationLogic = new RegistrationLogic();
        assertEquals(expected, registrationLogic.validateLogin(input));
    }
}