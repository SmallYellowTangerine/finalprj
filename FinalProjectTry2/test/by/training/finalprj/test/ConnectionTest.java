package by.training.finalprj.test;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.database.ConnectionPool;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Администратор on 18.08.2017.
 */
@RunWith(Parameterized.class)
public class ConnectionTest {

    private Set<Connection> usedConnections = Collections.synchronizedSet(new HashSet<>());

    @Parameters
    public static Collection<Object[]> connectionsAmount() {
        return Arrays.asList(new Object[][] {
                { 1, 1 }, { 2, 2 }, { 15, 15 }, { 31, 31 }, { 32, 32 }
        });
    }

    private int input;
    private int expected;

    public ConnectionTest(int input, int expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void getConnections() throws DAOException {
        for (int i = 0; i < input; i++) {
            usedConnections.add(ConnectionPool.getInstance().getConnection());
        }
        assertEquals(expected, usedConnections.size());
        for (Connection connection : usedConnections) {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }
}
