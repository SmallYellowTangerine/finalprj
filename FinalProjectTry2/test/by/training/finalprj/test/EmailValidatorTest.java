package by.training.finalprj.test;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.logic.RegistrationLogic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by Администратор on 06.09.2017.
 */
@RunWith(Parameterized.class)
public class EmailValidatorTest {

    @Parameterized.Parameters
    public static Collection<Object[]> connectionsAmount() {
        return Arrays.asList(new Object[][] {
                { "Vasya@gmail.com", true },
                { "12@dsf.ru", true },
                { "", false },
                { "vasya@gmailcom", false },
                { "Vasya.com", false }
        });
    }

    private String input;
    private Boolean expected;

    public EmailValidatorTest(String input, Boolean expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void validateEmail() throws DAOException {
        RegistrationLogic registrationLogic = new RegistrationLogic();
        assertEquals(expected, registrationLogic.validateEmail(input));
    }
}
