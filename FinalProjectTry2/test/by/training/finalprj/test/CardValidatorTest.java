package by.training.finalprj.test;

import by.training.finalprj.dao.DAOException;
import by.training.finalprj.logic.RegistrationLogic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by Администратор on 06.09.2017.
 */
@RunWith(Parameterized.class)
public class CardValidatorTest {

    @Parameterized.Parameters
    public static Collection<Object[]> connectionsAmount() {
        return Arrays.asList(new Object[][] {
                { "1234123412341234", true },
                { "123412341234abcd", false },
                { "", false },
                { "123412341234", false },
                { "1234123412341234123", false }
        });
    }

    private String input;
    private Boolean expected;

    public CardValidatorTest(String input, Boolean expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void validateCard() throws DAOException {
        RegistrationLogic registrationLogic = new RegistrationLogic();
        assertEquals(expected, registrationLogic.validateCard(input));
    }
}

