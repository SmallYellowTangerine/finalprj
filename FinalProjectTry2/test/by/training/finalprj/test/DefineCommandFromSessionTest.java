package by.training.finalprj.test;

import by.training.finalprj.manager.ParameterNameEnum;
import by.training.finalprj.command.factory.ActionFactory;
import by.training.finalprj.command.websocket.SendMessageCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Администратор on 25.08.2017.
 */
@RunWith(Parameterized.class)
public class DefineCommandFromSessionTest {
    @Parameterized.Parameters
    public static Collection<Object[]> commandNames() {
        return Arrays.asList(new Object[][] {
                {ParameterNameEnum.MESSAGING.getName(), SendMessageCommand.class}
        });
    }

    private String input;
    private Class expected;

    public DefineCommandFromSessionTest(String input, Class expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void defineCommandFromRequest() {
        Session session = new Session() {
            @Override
            public Map<String, Object> getUserProperties() {
                Map<String, Object> userProperties = new HashMap<>();
                userProperties.putIfAbsent(ParameterNameEnum.MESSAGING.getName(), input);
                return userProperties;
            }
            //<editor-fold desc="overriding required session methods">
            @Override
            public WebSocketContainer getContainer() {
                return null;
            }

            @Override
            public void addMessageHandler(MessageHandler messageHandler) throws IllegalStateException {

            }

            @Override
            public Set<MessageHandler> getMessageHandlers() {
                return null;
            }

            @Override
            public void removeMessageHandler(MessageHandler messageHandler) {

            }

            @Override
            public String getProtocolVersion() {
                return null;
            }

            @Override
            public String getNegotiatedSubprotocol() {
                return null;
            }

            @Override
            public List<Extension> getNegotiatedExtensions() {
                return null;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public boolean isOpen() {
                return false;
            }

            @Override
            public long getMaxIdleTimeout() {
                return 0;
            }

            @Override
            public void setMaxIdleTimeout(long l) {

            }

            @Override
            public void setMaxBinaryMessageBufferSize(int i) {

            }

            @Override
            public int getMaxBinaryMessageBufferSize() {
                return 0;
            }

            @Override
            public void setMaxTextMessageBufferSize(int i) {

            }

            @Override
            public int getMaxTextMessageBufferSize() {
                return 0;
            }

            @Override
            public RemoteEndpoint.Async getAsyncRemote() {
                return null;
            }

            @Override
            public RemoteEndpoint.Basic getBasicRemote() {
                return null;
            }

            @Override
            public String getId() {
                return null;
            }

            @Override
            public void close() throws IOException {

            }

            @Override
            public void close(CloseReason closeReason) throws IOException {

            }

            @Override
            public URI getRequestURI() {
                return null;
            }

            @Override
            public Map<String, List<String>> getRequestParameterMap() {
                return null;
            }

            @Override
            public String getQueryString() {
                return null;
            }

            @Override
            public Map<String, String> getPathParameters() {
                return null;
            }

            @Override
            public Principal getUserPrincipal() {
                return null;
            }

            @Override
            public Set<Session> getOpenSessions() {
                return null;
            }
            //</editor-fold>
        };
        ActionFactory actionFactory = new ActionFactory();
        Class result = actionFactory.defineCommand(session).getClass();
        assertEquals(expected, result);
    }
}
