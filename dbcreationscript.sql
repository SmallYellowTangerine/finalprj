-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema finalschema
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema finalschema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `finalschema` ;
USE `finalschema` ;

-- -----------------------------------------------------
-- Table `finalschema`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalschema`.`role` (
  `r_id` INT NOT NULL AUTO_INCREMENT,
  `r_rolename` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`r_id`),
  UNIQUE INDEX `r_rolename_UNIQUE` (`r_rolename` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `finalschema`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalschema`.`user` (
  `u_id` INT NOT NULL AUTO_INCREMENT,
  `u_login` VARCHAR(30) NOT NULL COMMENT 'Логин пользователя. Ограничение длины - просто для экономии памяти.',
  `u_password` CHAR(32) NOT NULL COMMENT 'Пароль предпологается хранить в sha_1, поэтому тип поля CHAR(40), чего достаточно для хранения таких паролей.',
  `u_email` VARCHAR(45) NOT NULL,
  `u_registration_date` DATE NULL COMMENT 'Дата регистрации пользователя',
  `u_role` INT NOT NULL,
  `u_bank_card_number` VARCHAR(16) NOT NULL,
  `u_balance` DECIMAL NULL,
  `u_rating` DOUBLE NULL DEFAULT 0.0,
  `u_games_won` INT NULL,
  `u_games_lost` INT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE INDEX `u_login_UNIQUE` (`u_login` ASC),
  INDEX `fk_user_role1_idx` (`u_role` ASC),
  UNIQUE INDEX `u_email_UNIQUE` (`u_email` ASC),
  INDEX `i_role_login` (`u_role` ASC, `u_login` ASC),
  CONSTRAINT `fk_user_role1`
    FOREIGN KEY (`u_role`)
    REFERENCES `finalschema`.`role` (`r_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `finalschema`.`game`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalschema`.`game` (
  `g_id` INT NOT NULL AUTO_INCREMENT,
  `g_start_date_time` DATETIME NULL COMMENT 'Дата (начала) игры',
  `g_winner` INT NULL COMMENT 'Победитель данной игры',
  `g_status` VARCHAR(45) NULL,
  PRIMARY KEY (`g_id`),
  INDEX `i_datetime` (`g_start_date_time` ASC),
  INDEX `g_status` (`g_status` ASC),
  INDEX `fk_game_user_idx` (`g_winner` ASC),
  CONSTRAINT `fk_game_user`
    FOREIGN KEY (`g_winner`)
    REFERENCES `finalschema`.`user` (`u_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `finalschema`.`game_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalschema`.`game_user` (
  `gu_id` INT NOT NULL AUTO_INCREMENT,
  `u_id` INT NOT NULL,
  `g_id` INT NOT NULL,
  PRIMARY KEY (`gu_id`),
  INDEX `fk_game_user_user1_idx` (`u_id` ASC),
  INDEX `fk_game_user_game1_idx` (`g_id` ASC),
  CONSTRAINT `fk_game_user_user1`
    FOREIGN KEY (`u_id`)
    REFERENCES `finalschema`.`user` (`u_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_game_user_game1`
    FOREIGN KEY (`g_id`)
    REFERENCES `finalschema`.`game` (`g_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `finalschema`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalschema`.`transaction` (
  `t_id` INT NOT NULL AUTO_INCREMENT,
  `t_amount` DECIMAL NOT NULL COMMENT 'Размер ставки (денежного перевода)',
  `t_date_time` DATETIME NULL COMMENT 'Точная дата и время',
  `t_is_from_user` TINYINT(1) NULL,
  `gu_id` INT NULL,
  PRIMARY KEY (`t_id`),
  INDEX `idx_player_received_amount` (`t_amount` ASC)  COMMENT 'Индекс для ситуации, когда нужно найти, например, все ставки игрока, или все выигрыши, и т.д.',
  INDEX `idx_amount` (`t_amount` ASC)  COMMENT 'Индекс для поиска по размеру ставок/переводов',
  INDEX `i_amount_is_from_user` (`t_amount` ASC, `t_is_from_user` ASC),
  INDEX `i_amount_game_player` (`t_amount` ASC),
  INDEX `fk_transaction_game_user1_idx` (`gu_id` ASC),
  CONSTRAINT `fk_transaction_game_user1`
    FOREIGN KEY (`gu_id`)
    REFERENCES `finalschema`.`game_user` (`gu_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `finalschema`.`message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `finalschema`.`message` (
  `m_id` INT NOT NULL AUTO_INCREMENT,
  `m_text` VARCHAR(1000) NULL COMMENT 'Текст сообщения',
  `m_date_time` DATETIME NULL COMMENT 'Дата и время отправки сообщения',
  `m_sender` INT NULL COMMENT 'Отправитель сообщения',
  `m_receiver` INT NULL COMMENT 'Получатель сообщения',
  `m_read` TINYINT(1) NULL,
  PRIMARY KEY (`m_id`),
  INDEX `fk_message_receiver_idx` (`m_receiver` ASC),
  INDEX `idx_date_time` (`m_date_time` ASC)  COMMENT 'Достаточно вероятно, что поиск может производиться по дате/времени.',
  INDEX `idx_sender_date_time` (`m_date_time` ASC)  COMMENT 'Достаточно вероятно, что может производиться поиск сообщений определенного пользователя в определенный день',
  INDEX `fk_message_sender_idx` (`m_sender` ASC),
  INDEX `i_sender_receiver` (`m_sender` ASC, `m_receiver` ASC),
  INDEX `i_receiver_sender` (`m_receiver` ASC, `m_sender` ASC),
  CONSTRAINT `fk_message_user2`
    FOREIGN KEY (`m_receiver`)
    REFERENCES `finalschema`.`user` (`u_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_message_user1`
    FOREIGN KEY (`m_sender`)
    REFERENCES `finalschema`.`user` (`u_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
